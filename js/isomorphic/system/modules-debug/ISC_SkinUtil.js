/*

  SmartClient Ajax RIA system
  Version v12.0p_2018-08-09/EVAL Development Only (2018-08-09)

  Copyright 2000 and beyond Isomorphic Software, Inc. All rights reserved.
  "SmartClient" is a trademark of Isomorphic Software, Inc.

  LICENSE NOTICE
     INSTALLATION OR USE OF THIS SOFTWARE INDICATES YOUR ACCEPTANCE OF
     ISOMORPHIC SOFTWARE LICENSE TERMS. If you have received this file
     without an accompanying Isomorphic Software license file, please
     contact licensing@isomorphic.com for details. Unauthorized copying and
     use of this software is a violation of international copyright law.

  DEVELOPMENT ONLY - DO NOT DEPLOY
     This software is provided for evaluation, training, and development
     purposes only. It may include supplementary components that are not
     licensed for deployment. The separate DEPLOY package for this release
     contains SmartClient components that are licensed for deployment.

  PROPRIETARY & PROTECTED MATERIAL
     This software contains proprietary materials that are protected by
     contract and intellectual property law. You are expressly prohibited
     from attempting to reverse engineer this software or modify this
     software for human readability.

  CONTACT ISOMORPHIC
     For more information regarding license rights and restrictions, or to
     report possible license violations, please contact Isomorphic Software
     by email (licensing@isomorphic.com) or web (www.isomorphic.com).

*/

if(window.isc&&window.isc.module_Core&&!window.isc.module_SkinUtil){isc.module_SkinUtil=1;isc._moduleStart=isc._SkinUtil_start=(isc.timestamp?isc.timestamp():new Date().getTime());if(isc._moduleEnd&&(!isc.Log||(isc.Log && isc.Log.logIsDebugEnabled('loadTime')))){isc._pTM={ message:'SkinUtil load/parse time: ' + (isc._moduleStart-isc._moduleEnd) + 'ms', category:'loadTime'};
if(isc.Log && isc.Log.logDebug)isc.Log.logDebug(isc._pTM.message,'loadTime');
else if(isc._preLog)isc._preLog[isc._preLog.length]=isc._pTM;
else isc._preLog=[isc._pTM]}isc.definingFramework=true;(function(){
function skinColor(color,elmnt){
  if(!(this instanceof skinColor)){return new skinColor(color,elmnt);}
  if(typeof color=="object"){return color;}
  this.attachValues(toColorObject(color));
  if(elmnt){elmnt.style.backgroundColor=this.toRgbString();}
}
skinColor.prototype={
  toRgbString:function(){
    return"rgb("+this.red+", "+this.green+", "+this.blue+")";
  },
  toRgbaString:function(){
    return"rgba("+this.red+", "+this.green+", "+this.blue+", "+this.opacity+")";
  },
  toHwbString:function(){
    return"hwb("+this.hue+", "+Math.round(this.whiteness*100)+"%, "+Math.round(this.blackness*100)+"%)";
  },
  toHwbStringDecimal:function(){
    return"hwb("+this.hue+", "+this.whiteness+", "+this.blackness+")";
  },
  toHwbaString:function(){
    return"hwba("+this.hue+", "+Math.round(this.whiteness*100)+"%, "+Math.round(this.blackness*100)+"%, "+this.opacity+")";
  },
  toHslString:function(){
    return"hsl("+this.hue+", "+Math.round(this.sat*100)+"%, "+Math.round(this.lightness*100)+"%)";
  },
  toHslStringDecimal:function(){
    return"hsl("+this.hue+", "+this.sat+", "+this.lightness+")";
  },
  toHslaString:function(){
    return"hsla("+this.hue+", "+Math.round(this.sat*100)+"%, "+Math.round(this.lightness*100)+"%, "+this.opacity+")";
  },
  toCmykString:function(){
    return"cmyk("+Math.round(this.cyan*100)+"%, "+Math.round(this.magenta*100)+"%, "+Math.round(this.yellow*100)+"%, "+Math.round(this.black*100)+"%)";
  },
  toCmykStringDecimal:function(){
    return"cmyk("+this.cyan+", "+this.magenta+", "+this.yellow+", "+this.black+")";
  },
  toNcolString:function(){
    return this.ncol+", "+Math.round(this.whiteness*100)+"%, "+Math.round(this.blackness*100)+"%";
  },
  toNcolStringDecimal:function(){
    return this.ncol+", "+this.whiteness+", "+this.blackness;
  },
  toNcolaString:function(){
    return this.ncol+", "+Math.round(this.whiteness*100)+"%, "+Math.round(this.blackness*100)+"%, "+this.opacity;
  },
  toName:function(){
    var r,g,b,colorhexs=getColorArr('hexs');
    for(i=0;i<colorhexs.length;i++){
      r=parseInt(colorhexs[i].substr(0,2),16);
      g=parseInt(colorhexs[i].substr(2,2),16);
      b=parseInt(colorhexs[i].substr(4,2),16);
      if(this.red==r&&this.green==g&&this.blue==b){
        return getColorArr('names')[i];
      }
    }
    return"";
  },
  toHexString:function(){
    var r=toHex(this.red);
    var g=toHex(this.green);
    var b=toHex(this.blue);
    return"#"+r+g+b;
  },
  toRgb:function(){
    return{r:this.red,g:this.green,b:this.blue,a:this.opacity};
  },
  toHsl:function(){
    return{h:this.hue,s:this.sat,l:this.lightness,a:this.opacity};
  },
  toHwb:function(){
    return{h:this.hue,w:this.whiteness,b:this.blackness,a:this.opacity};
  },
  toCmyk:function(){
    return{c:this.cyan,m:this.magenta,y:this.yellow,k:this.black,a:this.opacity};
  },
  toNcol:function(){
    return{ncol:this.ncol,w:this.whiteness,b:this.blackness,a:this.opacity};
  },
  isDark:function(n){
    var m=(n||128);
    return(((this.red*299+this.green*587+this.blue*114)/1000)<m);
  },
  saturate:function(n){
    var x,rgb,color;
    x=(n/100||0.1);
    this.sat+=x;
    if(this.sat>1){this.sat=1;}
    rgb=hslToRgb(this.hue,this.sat,this.lightness);
    color=colorObject(rgb,this.opacity,this.hue,this.sat);
    this.attachValues(color);
  },
  desaturate:function(n){
    var x,rgb,color;
    x=(n/100||0.1);
    this.sat-=x;
    if(this.sat<0){this.sat=0;}
    rgb=hslToRgb(this.hue,this.sat,this.lightness);
    color=colorObject(rgb,this.opacity,this.hue,this.sat);
    this.attachValues(color);
  },
  lighten:function(n){
    var x,rgb,color;
    x=(n/100||0.1);
    this.lightness+=x;
    if(this.lightness>1){this.lightness=1;}
    rgb=hslToRgb(this.hue,this.sat,this.lightness);
    color=colorObject(rgb,this.opacity,this.hue,this.sat);
    this.attachValues(color);
  },
  darken:function(n){
    var x,rgb,color;
    x=(n/100||0.1);
    this.lightness-=x;
    if(this.lightness<0){this.lightness=0;}
    rgb=hslToRgb(this.hue,this.sat,this.lightness);
    color=colorObject(rgb,this.opacity,this.hue,this.sat);
    this.attachValues(color);
  },
  attachValues:function(color){
    this.red=color.red;
    this.green=color.green;
    this.blue=color.blue;
    this.hue=color.hue;
    this.sat=color.sat;
    this.lightness=color.lightness;
    this.whiteness=color.whiteness;
    this.blackness=color.blackness;
    this.cyan=color.cyan;
    this.magenta=color.magenta;
    this.yellow=color.yellow;
    this.black=color.black;
    this.ncol=color.ncol;
    this.opacity=color.opacity;
    this.valid=color.valid;
  }
};
function toColorObject(c){
  var x,y,typ,arr=[],arrlength,i,opacity,match,a,hue,sat,rgb,colornames=[],colorhexs=[];
  c=w3trim(c.toLowerCase());
  x=c.substr(0,1).toUpperCase();
  y=c.substr(1);
  a=1;
  if((x=="R"||x=="Y"||x=="G"||x=="C"||x=="B"||x=="M"||x=="W")&&!isNaN(y)){c="ncol("+c+")";}
  if(c.length!=3&&c.length!=6&&!isNaN(c)){c="ncol("+c+")";}
  if(c.indexOf(",")>0&&c.indexOf("(")==-1){c="ncol("+c+")";}
  if(c.substr(0,3)=="rgb"||c.substr(0,3)=="hsl"||c.substr(0,3)=="hwb"||c.substr(0,4)=="ncol"||c.substr(0,4)=="cmyk"){
    if(c.substr(0,4)=="ncol"){
      if(c.split(",").length==4&&c.indexOf("ncola")==-1){
        c=c.replace("ncol","ncola");
      }
      typ="ncol";
      c=c.substr(4);
    }else if(c.substr(0,4)=="cmyk"){
      typ="cmyk";
      c=c.substr(4);
    }else{
      typ=c.substr(0,3);
      c=c.substr(3);
    }
    arrlength=3;
    opacity=false;
    if(c.substr(0,1).toLowerCase()=="a"){
      arrlength=4;
      opacity=true;
      c=c.substr(1);
    }else if(typ=="cmyk"){
      arrlength=4;
      if(c.split(",").length==5){
        arrlength=5;
        opacity=true;
      }
    }
    c=c.replace("(","");
    c=c.replace(")","");
    arr=c.split(",");
    if(typ=="rgb"){
      if(arr.length!=arrlength){
        return emptyObject();
      }
      for(i=0;i<arrlength;i++){
        if(arr[i]==""||arr[i]==" "){arr[i]="0";}
        if(arr[i].indexOf("%")>-1){
          arr[i]=arr[i].replace("%","");
          arr[i]=Number(arr[i]/100);
          if(i<3){arr[i]=Math.round(arr[i]*255);}
        }
        if(isNaN(arr[i])){return emptyObject();}
        if(parseInt(arr[i])>255){arr[i]=255;}
        if(i<3){arr[i]=parseInt(arr[i]);}
        if(i==3&&Number(arr[i])>1){arr[i]=1;}
      }
      rgb={r:arr[0],g:arr[1],b:arr[2]};
      if(opacity==true){a=Number(arr[3]);}
    }
    if(typ=="hsl"||typ=="hwb"||typ=="ncol"){
      while(arr.length<arrlength){arr.push("0");}
      if(typ=="hsl"||typ=="hwb"){
        if(parseInt(arr[0])>=360){arr[0]=0;}
      }
      for(i=1;i<arrlength;i++){
        if(arr[i].indexOf("%")>-1){
          arr[i]=arr[i].replace("%","");
          arr[i]=Number(arr[i]);
          if(isNaN(arr[i])){return emptyObject();}
          arr[i]=arr[i]/100;
        }else{
          arr[i]=Number(arr[i]);
        }
        if(Number(arr[i])>1){arr[i]=1;}
        if(Number(arr[i])<0){arr[i]=0;}
      }
      if(typ=="hsl"){rgb=hslToRgb(arr[0],arr[1],arr[2]);hue=Number(arr[0]);sat=Number(arr[1]);}
      if(typ=="hwb"){rgb=hwbToRgb(arr[0],arr[1],arr[2]);}
      if(typ=="ncol"){rgb=ncolToRgb(arr[0],arr[1],arr[2]);}
      if(opacity==true){a=Number(arr[3]);}
    }
    if(typ=="cmyk"){
      while(arr.length<arrlength){arr.push("0");}
      for(i=0;i<arrlength;i++){
        if(arr[i].indexOf("%")>-1){
          arr[i]=arr[i].replace("%","");
          arr[i]=Number(arr[i]);
          if(isNaN(arr[i])){return emptyObject();}
          arr[i]=arr[i]/100;
        }else{
          arr[i]=Number(arr[i]);
        }
        if(Number(arr[i])>1){arr[i]=1;}
        if(Number(arr[i])<0){arr[i]=0;}
      }
      rgb=cmykToRgb(arr[0],arr[1],arr[2],arr[3]);
      if(opacity==true){a=Number(arr[4]);}
    }
  }else if(c.substr(0,3)=="ncs"){
    rgb=ncsToRgb(c);
  }else{
    match=false;
    colornames=getColorArr('names');
    for(i=0;i<colornames.length;i++){
      if(c.toLowerCase()==colornames[i].toLowerCase()){
        colorhexs=getColorArr('hexs');
        match=true;
        rgb={
          r:parseInt(colorhexs[i].substr(0,2),16),
          g:parseInt(colorhexs[i].substr(2,2),16),
          b:parseInt(colorhexs[i].substr(4,2),16)
        };
        break;
      }
    }
    if(match==false){
      c=c.replace("#","");
      if(c.length==3){c=c.substr(0,1)+c.substr(0,1)+c.substr(1,1)+c.substr(1,1)+c.substr(2,1)+c.substr(2,1);}
      arr[0]=parseInt(c.substr(0,2),16);
      arr[1]=parseInt(c.substr(2,2),16);
      arr[2]=parseInt(c.substr(4,2),16);
      for(i=0;i<3;i++){
        if(isNaN(arr[i])){return emptyObject();}
      }
      rgb={
        r:arr[0],
        g:arr[1],
        b:arr[2]
      };
    }
  }
  return colorObject(rgb,a,hue,sat);
}
function colorObject(rgb,a,h,s){
  var hsl,hwb,cmyk,ncol,color,hue,sat;
  if(!rgb){return emptyObject();}
  if(!a){a=1;}
  hsl=rgbToHsl(rgb.r,rgb.g,rgb.b);
  hwb=rgbToHwb(rgb.r,rgb.g,rgb.b);
  cmyk=rgbToCmyk(rgb.r,rgb.g,rgb.b);
  hue=(h||hsl.h);
  sat=(s||hsl.s);
  ncol=hueToNcol(hue);
  color={
    red:rgb.r,
    green:rgb.g,
    blue:rgb.b,
    hue:hue,
    sat:sat,
    lightness:hsl.l,
    whiteness:hwb.w,
    blackness:hwb.b,
    cyan:cmyk.c,
    magenta:cmyk.m,
    yellow:cmyk.y,
    black:cmyk.k,
    ncol:ncol,
    opacity:a,
    valid:true
  };
  color=roundDecimals(color);
  return color;
}
function emptyObject(){
  return{
    red:0,
    green:0,
    blue:0,
    hue:0,
    sat:0,
    lightness:0,
    whiteness:0,
    blackness:0,
    cyan:0,
    magenta:0,
    yellow:0,
    black:0,
    ncol:"R",
    opacity:1,
    valid:false
  };
}
function getColorArr(x){
  if(x=="names"){return['AliceBlue','AntiqueWhite','Aqua','Aquamarine','Azure','Beige','Bisque','Black','BlanchedAlmond','Blue','BlueViolet','Brown','BurlyWood','CadetBlue','Chartreuse','Chocolate','Coral','CornflowerBlue','Cornsilk','Crimson','Cyan','DarkBlue','DarkCyan','DarkGoldenRod','DarkGray','DarkGrey','DarkGreen','DarkKhaki','DarkMagenta','DarkOliveGreen','DarkOrange','DarkOrchid','DarkRed','DarkSalmon','DarkSeaGreen','DarkSlateBlue','DarkSlateGray','DarkSlateGrey','DarkTurquoise','DarkViolet','DeepPink','DeepSkyBlue','DimGray','DimGrey','DodgerBlue','FireBrick','FloralWhite','ForestGreen','Fuchsia','Gainsboro','GhostWhite','Gold','GoldenRod','Gray','Grey','Green','GreenYellow','HoneyDew','HotPink','IndianRed','Indigo','Ivory','Khaki','Lavender','LavenderBlush','LawnGreen','LemonChiffon','LightBlue','LightCoral','LightCyan','LightGoldenRodYellow','LightGray','LightGrey','LightGreen','LightPink','LightSalmon','LightSeaGreen','LightSkyBlue','LightSlateGray','LightSlateGrey','LightSteelBlue','LightYellow','Lime','LimeGreen','Linen','Magenta','Maroon','MediumAquaMarine','MediumBlue','MediumOrchid','MediumPurple','MediumSeaGreen','MediumSlateBlue','MediumSpringGreen','MediumTurquoise','MediumVioletRed','MidnightBlue','MintCream','MistyRose','Moccasin','NavajoWhite','Navy','OldLace','Olive','OliveDrab','Orange','OrangeRed','Orchid','PaleGoldenRod','PaleGreen','PaleTurquoise','PaleVioletRed','PapayaWhip','PeachPuff','Peru','Pink','Plum','PowderBlue','Purple','RebeccaPurple','Red','RosyBrown','RoyalBlue','SaddleBrown','Salmon','SandyBrown','SeaGreen','SeaShell','Sienna','Silver','SkyBlue','SlateBlue','SlateGray','SlateGrey','Snow','SpringGreen','SteelBlue','Tan','Teal','Thistle','Tomato','Turquoise','Violet','Wheat','White','WhiteSmoke','Yellow','YellowGreen'];}
  if(x=="hexs"){return['f0f8ff','faebd7','00ffff','7fffd4','f0ffff','f5f5dc','ffe4c4','000000','ffebcd','0000ff','8a2be2','a52a2a','deb887','5f9ea0','7fff00','d2691e','ff7f50','6495ed','fff8dc','dc143c','00ffff','00008b','008b8b','b8860b','a9a9a9','a9a9a9','006400','bdb76b','8b008b','556b2f','ff8c00','9932cc','8b0000','e9967a','8fbc8f','483d8b','2f4f4f','2f4f4f','00ced1','9400d3','ff1493','00bfff','696969','696969','1e90ff','b22222','fffaf0','228b22','ff00ff','dcdcdc','f8f8ff','ffd700','daa520','808080','808080','008000','adff2f','f0fff0','ff69b4','cd5c5c','4b0082','fffff0','f0e68c','e6e6fa','fff0f5','7cfc00','fffacd','add8e6','f08080','e0ffff','fafad2','d3d3d3','d3d3d3','90ee90','ffb6c1','ffa07a','20b2aa','87cefa','778899','778899','b0c4de','ffffe0','00ff00','32cd32','faf0e6','ff00ff','800000','66cdaa','0000cd','ba55d3','9370db','3cb371','7b68ee','00fa9a','48d1cc','c71585','191970','f5fffa','ffe4e1','ffe4b5','ffdead','000080','fdf5e6','808000','6b8e23','ffa500','ff4500','da70d6','eee8aa','98fb98','afeeee','db7093','ffefd5','ffdab9','cd853f','ffc0cb','dda0dd','b0e0e6','800080','663399','ff0000','bc8f8f','4169e1','8b4513','fa8072','f4a460','2e8b57','fff5ee','a0522d','c0c0c0','87ceeb','6a5acd','708090','708090','fffafa','00ff7f','4682b4','d2b48c','008080','d8bfd8','ff6347','40e0d0','ee82ee','f5deb3','ffffff','f5f5f5','ffff00','9acd32'];}
}
function roundDecimals(c){
  c.red=Number(c.red.toFixed(0));
  c.green=Number(c.green.toFixed(0));
  c.blue=Number(c.blue.toFixed(0));
  c.hue=Number(c.hue.toFixed(0));
  c.sat=Number(c.sat.toFixed(2));
  c.lightness=Number(c.lightness.toFixed(2));
  c.whiteness=Number(c.whiteness.toFixed(2));
  c.blackness=Number(c.blackness.toFixed(2));
  c.cyan=Number(c.cyan.toFixed(2));
  c.magenta=Number(c.magenta.toFixed(2));
  c.yellow=Number(c.yellow.toFixed(2));
  c.black=Number(c.black.toFixed(2));
  c.ncol=c.ncol.substr(0,1)+Math.round(Number(c.ncol.substr(1)));
  c.opacity=Number(c.opacity.toFixed(2));
  return c;
}
function hslToRgb(hue,sat,light){
  var t1,t2,r,g,b;
  hue=hue/60;
  if(light<=0.5){
    t2=light*(sat+1);
  }else{
    t2=light+sat-(light*sat);
  }
  t1=light*2-t2;
  r=hueToRgb(t1,t2,hue+2)*255;
  g=hueToRgb(t1,t2,hue)*255;
  b=hueToRgb(t1,t2,hue-2)*255;
  return{r:r,g:g,b:b};
}
function hueToRgb(t1,t2,hue){
  if(hue<0)hue+=6;
  if(hue>=6)hue-=6;
  if(hue<1)return(t2-t1)*hue+t1;
  else if(hue<3)return t2;
  else if(hue<4)return(t2-t1)*(4-hue)+t1;
  else return t1;
}
function hwbToRgb(hue,white,black){
  var i,rgb,rgbArr=[];
  rgb=hslToRgb(hue,1,0.50);
  rgbArr[0]=rgb.r/255;
  rgbArr[1]=rgb.g/255;
  rgbArr[2]=rgb.b/255;
  for(i=0;i<3;i++){
    rgbArr[i]*=(1-(white)-(black));
    rgbArr[i]+=(white);
    rgbArr[i]=Number(rgbArr[i]*255);
  }
  return{r:rgbArr[0],g:rgbArr[1],b:rgbArr[2]};
}
function cmykToRgb(c,m,y,k){
  var r,g,b;
  r=255-((Math.min(1,c*(1-k)+k))*255);
  g=255-((Math.min(1,m*(1-k)+k))*255);
  b=255-((Math.min(1,y*(1-k)+k))*255);
  return{r:r,g:g,b:b};
}
function ncolToRgb(ncol,white,black){
  var letter,percent,h,w,b;
  h=ncol;
  if(isNaN(ncol.substr(0,1))){
    letter=ncol.substr(0,1).toUpperCase();
    percent=ncol.substr(1);
    if(percent==""){percent=0;}
    percent=Number(percent);
    if(isNaN(percent)){return false;}
    if(letter=="R"){h=0+(percent*0.6);}
    if(letter=="Y"){h=60+(percent*0.6);}
    if(letter=="G"){h=120+(percent*0.6);}
    if(letter=="C"){h=180+(percent*0.6);}
    if(letter=="B"){h=240+(percent*0.6);}
    if(letter=="M"){h=300+(percent*0.6);}
    if(letter=="W"){
      h=0;
      white=1-(percent/100);
      black=(percent/100);
    }
  }
  return hwbToRgb(h,white,black);
}
function hueToNcol(hue){
  while(hue>=360){
    hue=hue-360;
  }
  if(hue<60){return"R"+(hue/0.6);}
  if(hue<120){return"Y"+((hue-60)/0.6);}
  if(hue<180){return"G"+((hue-120)/0.6);}
  if(hue<240){return"C"+((hue-180)/0.6);}
  if(hue<300){return"B"+((hue-240)/0.6);}
  if(hue<360){return"M"+((hue-300)/0.6);}
}
function ncsToRgb(ncs){
  var black,chroma,bc,percent,black1,chroma1,red1,factor1,blue1,red1,red2,green2,blue2,max,factor2,grey,r,g,b;
  ncs=w3trim(ncs).toUpperCase();
  ncs=ncs.replace("(","");
  ncs=ncs.replace(")","");
  ncs=ncs.replace("NCS","NCS ");
  ncs=ncs.replace(/  /g," ");
  if(ncs.indexOf("NCS")==-1){ncs="NCS "+ncs;}
  ncs=ncs.match(/^(?:NCS|NCS\sS)\s(\d{2})(\d{2})-(N|[A-Z])(\d{2})?([A-Z])?$/);
  if(ncs===null)return false;
  black=parseInt(ncs[1],10);
  chroma=parseInt(ncs[2],10);
  bc=ncs[3];
  if(bc!="N"&&bc!="Y"&&bc!="R"&&bc!="B"&&bc!="G"){return false;}
  percent=parseInt(ncs[4],10)||0;
  if(bc!=='N'){
    black1=(1.05*black-5.25);
    chroma1=chroma;
    if(bc==='Y'&&percent<=60){
      red1=1;
    }else if((bc==='Y'&&percent>60)||(bc==='R'&&percent<=80)){
      if(bc==='Y'){
        factor1=percent-60;
      }else{
        factor1=percent+40;
      }
      red1=((Math.sqrt(14884-Math.pow(factor1,2)))-22)/100;
    }else if((bc==='R'&&percent>80)||(bc==='B')){
      red1=0;
    }else if(bc==='G'){
      factor1=(percent-170);
      red1=((Math.sqrt(33800-Math.pow(factor1,2)))-70)/100;
    }
    if(bc==='Y'&&percent<=80){
      blue1=0;
    }else if((bc==='Y'&&percent>80)||(bc==='R'&&percent<=60)){
      if(bc==='Y'){
        factor1=(percent-80)+20.5;
      }else{
        factor1=(percent+20)+20.5;
      }
      blue1=(104-(Math.sqrt(11236-Math.pow(factor1,2))))/100;
    }else if((bc==='R'&&percent>60)||(bc==='B'&&percent<=80)){
      if(bc==='R'){
        factor1=(percent-60)-60;
      }else{
        factor1=(percent+40)-60;
      }
      blue1=((Math.sqrt(10000-Math.pow(factor1,2)))-10)/100;
    }else if((bc==='B'&&percent>80)||(bc==='G'&&percent<=40)){
      if(bc==='B'){
        factor1=(percent-80)-131;
      }else{
        factor1=(percent+20)-131;
      }
      blue1=(122-(Math.sqrt(19881-Math.pow(factor1,2))))/100;
    }else if(bc==='G'&&percent>40){
      blue1=0;
    }
    if(bc==='Y'){
      green1=(85-17/20*percent)/100;
    }else if(bc==='R'&&percent<=60){
      green1=0;
    }else if(bc==='R'&&percent>60){
      factor1=(percent-60)+35;
      green1=(67.5-(Math.sqrt(5776-Math.pow(factor1,2))))/100;
    }else if(bc==='B'&&percent<=60){
      factor1=(1*percent-68.5);
      green1=(6.5+(Math.sqrt(7044.5-Math.pow(factor1,2))))/100;
    }else if((bc==='B'&&percent>60)||(bc==='G'&&percent<=60)){
      green1=0.9;
    }else if(bc==='G'&&percent>60){
      factor1=(percent-60);
      green1=(90-(1/8*factor1))/100;
    }
    factor1=(red1+green1+blue1)/3;
    red2=((factor1-red1)*(100-chroma1)/100)+red1;
    green2=((factor1-green1)*(100-chroma1)/100)+green1;
    blue2=((factor1-blue1)*(100-chroma1)/100)+blue1;
    if(red2>green2&&red2>blue2){
      max=red2;
    }else if(green2>red2&&green2>blue2){
      max=green2;
    }else if(blue2>red2&&blue2>green2){
      max=blue2;
    }else{
      max=(red2+green2+blue2)/3;
    }
    factor2=1/max;
    r=parseInt((red2*factor2*(100-black1)/100)*255,10);
    g=parseInt((green2*factor2*(100-black1)/100)*255,10);
    b=parseInt((blue2*factor2*(100-black1)/100)*255,10);
    if(r>255){r=255;}
    if(g>255){g=255;}
    if(b>255){b=255;}
    if(r<0){r=0;}
    if(g<0){g=0;}
    if(b<0){b=0;}
  }else{
    grey=parseInt((1-black/100)*255,10);
    if(grey>255){grey=255;}
    if(grey<0){grey=0;}
    r=grey;
    g=grey;
    b=grey;
  }
  return{
    r:r,
    g:g,
    b:b
  };
}
function rgbToHsl(r,g,b){
  var min,max,i,l,s,maxcolor,h,rgb=[];
  rgb[0]=r/255;
  rgb[1]=g/255;
  rgb[2]=b/255;
  min=rgb[0];
  max=rgb[0];
  maxcolor=0;
  for(i=0;i<rgb.length-1;i++){
    if(rgb[i+1]<=min){min=rgb[i+1];}
    if(rgb[i+1]>=max){max=rgb[i+1];maxcolor=i+1;}
  }
  if(maxcolor==0){
    h=(rgb[1]-rgb[2])/(max-min);
  }
  if(maxcolor==1){
    h=2+(rgb[2]-rgb[0])/(max-min);
  }
  if(maxcolor==2){
    h=4+(rgb[0]-rgb[1])/(max-min);
  }
  if(isNaN(h)){h=0;}
  h=h*60;
  if(h<0){h=h+360;}
  l=(min+max)/2;
  if(min==max){
    s=0;
  }else{
    if(l<0.5){
      s=(max-min)/(max+min);
    }else{
      s=(max-min)/(2-max-min);
    }
  }
  s=s;
  return{h:h,s:s,l:l};
}
function rgbToHwb(r,g,b){
  var h,w,bl;
  r=r/255;
  g=g/255;
  b=b/255;
  max=Math.max(r,g,b);
  min=Math.min(r,g,b);
  chroma=max-min;
  if(chroma==0){
    h=0;
  }else if(r==max){
    h=(((g-b)/chroma)%6)*360;
  }else if(g==max){
    h=((((b-r)/chroma)+2)%6)*360;
  }else{
    h=((((r-g)/chroma)+4)%6)*360;
  }
  w=min;
  bl=1-max;
  return{h:h,w:w,b:bl};
}
function rgbToCmyk(r,g,b){
  var c,m,y,k;
  r=r/255;
  g=g/255;
  b=b/255;
  max=Math.max(r,g,b);
  k=1-max;
  if(k==1){
    c=0;
    m=0;
    y=0;
  }else{
    c=(1-r-k)/(1-k);
    m=(1-g-k)/(1-k);
    y=(1-b-k)/(1-k);
  }
  return{c:c,m:m,y:y,k:k};
}
function toHex(n){
  var hex=n.toString(16);
  while(hex.length<2){hex="0"+hex;}
  return hex;
}
function cl(x){
  console.log(x);
}
function w3trim(x){
  return x.replace(/^\s+|\s+$/g,'');
}
window.skinColor=skinColor;
function rgbToXyz(r,g,b){
    var _r=(r/255);
    var _g=(g/255);
    var _b=(b/255);
    if(_r>0.04045){
        _r=Math.pow(((_r+0.055)/1.055),2.4);
    }
    else{
        _r=_r/12.92;
    }
    if(_g>0.04045){
        _g=Math.pow(((_g+0.055)/1.055),2.4);
    }
    else{
        _g=_g/12.92;
    }
    if(_b>0.04045){
        _b=Math.pow(((_b+0.055)/1.055),2.4);
    }
    else{
        _b=_b/12.92;
    }
    _r=_r*100;
    _g=_g*100;
    _b=_b*100;
    X=_r*0.4124+_g*0.3576+_b*0.1805;
    Y=_r*0.2126+_g*0.7152+_b*0.0722;
    Z=_r*0.0193+_g*0.1192+_b*0.9505;
    return[X,Y,Z];
};
function rgbTolab(r,g,b){
    return xyzToLab(rbgToXyz(r,g,b));
}
function xyzToLab(x,y,z){
    var ref_X=95.047;
    var ref_Y=100.000;
    var ref_Z=108.883;
    var _X=x/ref_X;
    var _Y=y/ref_Y;
    var _Z=z/ref_Z;
    if(_X>0.008856){
         _X=Math.pow(_X,(1/3));
    }
    else{
        _X=(7.787*_X)+(16/116);
    }
    if(_Y>0.008856){
        _Y=Math.pow(_Y,(1/3));
    }
    else{
      _Y=(7.787*_Y)+(16/116);
    }
    if(_Z>0.008856){
        _Z=Math.pow(_Z,(1/3));
    }
    else{
        _Z=(7.787*_Z)+(16/116);
    }
    var CIE_L=(116*_Y)-16;
    var CIE_a=500*(_X-_Y);
    var CIE_b=200*(_Y-_Z);
    return[CIE_L,CIE_a,CIE_b];
};
function cie1994(x,y,isTextiles){
    var x={l:x[0],a:x[1],b:x[2]};
    var y={l:y[0],a:y[1],b:y[2]};
    labx=x;
    laby=y;
    var k2;
    var k1;
    var kl;
    var kh=1;
    var kc=1;
    if(isTextiles){
        k2=0.014;
        k1=0.048;
        kl=2;
    }
    else{
        k2=0.015;
        k1=0.045;
        kl=1;
    }
    var c1=Math.sqrt(x.a*x.a+x.b*x.b);
    var c2=Math.sqrt(y.a*y.a+y.b*y.b);
    var sh=1+k2*c1;
    var sc=1+k1*c1;
    var sl=1;
    var da=x.a-y.a;
    var db=x.b-y.b;
    var dc=c1-c2;
    var dl=x.l-y.l;
    var dh=Math.sqrt(da*da+db*db-dc*dc);
    return Math.sqrt(Math.pow((dl/(kl*sl)),2)+Math.pow((dc/(kc*sc)),2)+Math.pow((dh/(kh*sh)),2));
};
})();
isc.defineClass("SkinUtil");
isc.A=isc.SkinUtil;
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.colorArrays=["background","color","border","borderTop","borderLeft","borderBottom","borderRight"];
isc.A.skinColorWindowDefaults={
        _constructor:"Window",
        autoDraw:false,
        autoCenter:true,
        overflow:"visible",
        canDragResize:true,
        title:"Palette Tools",
        colorEditorDefaults:{
            _constructor:"SkinPaletteTools"
        },
        bodyProperties:{overflow:"visible"},
        draw:function(){
            this.Super("draw",arguments);
            this.addAutoChild("colorEditor");
            this.body.addMember(this.colorEditor);
        }
    };
isc.B.push(isc.A.toHexString=function isc_c_SkinUtil_toHexString(color){
        var result=skinColor(color);
        if(result.valid)return result.toHexString();
        else return"";
    }
,isc.A.getSkinColor=function isc_c_SkinUtil_getSkinColor(color){
        return skinColor(color);
    }
,isc.A.createDataSources=function isc_c_SkinUtil_createDataSources(){
        var data=isc.clone(isc.Canvas._currentColorMap);
        var msg="";
        var searchData=[];
        for(var i=0;i<data.length;i++){
            var color=data[i];
            color.backgroundCount=color.background.length;
            color.colorCount=color.color.length;
            color.borderCount=color.border.length;
            color.borderTopCount=color.borderTop.length;
            color.borderLeftCount=color.borderLeft.length;
            color.borderBottomCount=color.borderBottom.length;
            color.borderRightCount=color.borderRight.length;
            var skinColor=isc.SkinUtil.getSkinColor(color.original);
            isc.addProperties(color,skinColor);
            msg+="\n"+color.original+
                " background: "+color.backgroundCount+
                ", font-color: "+color.colorCount+
                ", border: "+color.borderCount+
                ", borderTop: "+color.borderTopCount+
                ", borderLeft: "+color.borderLeftCount+
                ", borderBottom: "+color.borderBottomCount+
                ", borderRight: "+color.borderRightCount
            ;
            var maxType=0;
            var colorArrays=isc.SkinUtil.colorArrays;
            for(var j=0;j<colorArrays.length;j++){
                var typeCount=color[colorArrays[j]+"Count"];
                if(typeCount>maxType){
                    maxType=typeCount;
                    if(colorArrays[j].contains("border")){
                        color.mostlyType="Borders";
                    }else if(colorArrays[j].contains("background")){
                        color.mostlyType="Backgrounds";
                    }else{
                        color.mostlyType="Fonts";
                    }
                }
            }
            searchData.add(color);
        }
        isc.DataSource.create({
            ID:"colorSearchDS",
            clientOnly:true,
            fields:[
                {name:"original",primaryKey:true},
                {name:"current"},
                {name:"isSkinCSS",type:"boolean"},
                {name:"background",multiple:true},
                {name:"color",multiple:true},
                {name:"border",multiple:true},
                {name:"borderTop",multiple:true},
                {name:"borderLeft",multiple:true},
                {name:"borderBottom",multiple:true},
                {name:"borderRight",multiple:true},
                {name:"backgroundCount",type:"integer"},
                {name:"colorCount",type:"integer"},
                {name:"borderCount",type:"integer"},
                {name:"borderTopCount",type:"integer"},
                {name:"borderLeftCount",type:"integer"},
                {name:"borderBottomCount",type:"integer"},
                {name:"borderRightCount",type:"integer"},
                {name:"mostlyType"},
                {name:"red",type:"integer"},
                {name:"green",type:"integer"},
                {name:"blue",type:"integer"},
                {name:"hue",type:"integer"},
                {name:"sat",type:"integer"},
                {name:"lightness",type:"integer"},
                {name:"lab",type:"number"}
            ],
            cacheData:searchData
        });
        isc.SkinUtil.colorSearchDS=colorSearchDS;
    }
,isc.A.showSkinPaletteTools=function isc_c_SkinUtil_showSkinPaletteTools(refreshPalette){
        if(refreshPalette||!isc.Canvas._currentColorMap)isc.Canvas.getSkinColors();
        if(isc.SkinUtil._skinColorWindow==null){
            isc.SkinUtil._skinColorWindow=isc.Window.create(isc.SkinUtil.skinColorWindowDefaults);
        }
        var win=isc.SkinUtil._skinColorWindow;
        win.setTitle("Palette Tools - "+isc.getCurrentSkinName());
        win.show();
        return win;
    }
,isc.A.getClosestColors=function isc_c_SkinUtil_getClosestColors(color,maxDistance){
        var SU=isc.SkinUtil,
            ds=SU.colorSearchDS,
            data=ds.cacheData,
            results=[]
        ;
        if(!maxDistance)maxDistance=10;
        for(var i=0;i<data.length;i++){
            var otherColor=data[i].current;
            if(color==otherColor)continue;
            var diff=SU.colorDistance(color,otherColor);
            if(diff<=maxDistance){
                results.add(data[i]);
            }
        }
        return results;
    }
,isc.A.colorDistance=function isc_c_SkinUtil_colorDistance(color1,color2){
        var skinColor1=isc.SkinUtil.getSkinColor(color1);
        var skinColor2=isc.SkinUtil.getSkinColor(color2);
        var rmean=(skinColor1.red+skinColor2.red)/2;
        var r=skinColor1.red-skinColor2.red;
        var g=skinColor1.green-skinColor2.green;
        var b=skinColor1.blue-skinColor2.blue;
        return Math.sqrt((((512+rmean)*r*r)>>8)+4*g*g+(((767-rmean)*b*b)>>8));
    }
,isc.A.getContrastYIQ=function isc_c_SkinUtil_getContrastYIQ(hexcolor){
        var r=parseInt(hexcolor.substr(0,2),16);
        var g=parseInt(hexcolor.substr(2,2),16);
        var b=parseInt(hexcolor.substr(4,2),16);
        var yiq=((r*299)+(g*587)+(b*114))/1000;
        return(yiq>=128)?'black':'white';
    }
);
isc.B._maxIndex=isc.C+7;

isc.defineClass("SkinColorPalette","Canvas");
isc.A=isc.SkinColorPalette.getPrototype();
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.overflow="visible";
isc.A.titleLabelDefaults={
        _constructor:"Canvas",
        autoDraw:true,
        autoSize:true,
        wrap:false,
        height:20,
        overflow:"visible"
    };
isc.B.push(isc.A.initWidget=function isc_SkinColorPalette_initWidget(){
        this.Super("initWidget",arguments);
        this.addAutoChild("titleLabel");
        this.addChild(this.titleLabel);
        if(this.title)this.titleLabel.setContents(this.title);
        this.titleLabel.redraw();
        this.drawColors();
    }
,isc.A.selectColor=function isc_SkinColorPalette_selectColor(color){
        var canvas=null;
        if(this.selectedCanvas){
            this.selectedCanvas.setProperty("border","2px solid "+this.selectedCanvas.color);
        }
        for(var i=0;i<this.children.length;i++){
            if(this.children[i].isColorCanvas&&this.children[i].color==color){
                this.selectedCanvas=this.children[i];
                this.selectedCanvas.setProperty("border","2px solid white");
                break;
            }
        }
    }
,isc.A.drawColors=function isc_SkinColorPalette_drawColors(colors){
        if(this.children){
            for(var i=this.children.length-1;i>0;i--){
                var canvas=this.children[i];
                if(canvas.isColorCanvas){
                    this.removeChild(canvas);
                    canvas.destroy();
                    canvas=null;
                }
            }
        }
        colors=colors||this.colors||isc.Canvas._currentColorMap.findAll({isSkinCSS:true});
        colors.setSort([
            {property:"original",direction:"ascending"}
        ]);
        var labelHeight=this.titleLabel.getHeight();
        var top=0,left=0;
        var row=0,col=0;
        for(var i=0;i<colors.length;i++){
            if(col==10){
                col=0;
                row++
            }
            var canvas=this.getColorCanvas(colors[i].original);
            this.addChild(canvas)
            canvas.moveTo(col*20,labelHeight+(row*20));
            col++;
        }
    }
,isc.A.getColorCanvas=function isc_SkinColorPalette_getColorCanvas(color){
        return isc.Canvas.create({
            width:20,height:20,
            color:color,
            isColorCanvas:true,
            autoDraw:false,
            border:"2px solid "+color,
            backgroundColor:color,
            prompt:color
        });
    }
);
isc.B._maxIndex=isc.C+4;

isc.defineClass("ColorConverter","VLayout");
isc.A=isc.ColorConverter.getPrototype();
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.width=270;
isc.A.height=180;
isc.A.overflow="auto";
isc.A.autoDraw=true;
isc.A.formDefaults={
        _constructor:"DynamicForm",
        colWidths:[100,150],
        fields:[
            {name:"inputColor",width:"*",
                changed:function(form,item,value){
                    form.creator.setColor(value);
                }
            },
            {name:"changeTo",editorType:"ColorItem",title:"Change To",width:"*",
                changed:function(form,item,value){
                    form.creator.changeColor(form.getValue("Hex"),value);
                }
            },
            {name:"Hex",editorType:"ColorItem",width:"*"},
            {name:"Name",width:"*"},
            {name:"Rgb",width:"*"},
            {name:"Hsl",width:"*"},
            {name:"Hwb",width:"*"},
            {name:"Cmyk",width:"*"}
        ]
    };
isc.B.push(isc.A.initWidget=function isc_ColorConverter_initWidget(){
        this.Super("initWidget",arguments);
        this.addAutoChild("form");
        this.addMember(this.form);
        if(this.color)this.setColor(this.color);
    }
,isc.A.setColor=function isc_ColorConverter_setColor(color){
        this.inputColor=color;
        this.color=isc.SkinUtil.getSkinColor(color);
        this.updateForm();
    }
,isc.A.changeColor=function isc_ColorConverter_changeColor(oldColor,newColor){
        var props={};
        props[oldColor]=newColor;
        isc.Canvas.changeSkinColors(props);
        this.creator.changeConfigColor(oldColor,newColor);
    }
,isc.A.updateForm=function isc_ColorConverter_updateForm(){
        var c=this.color;
        if(c.valid){
            var hexColor=c.toHexString();
            var skinColor=isc.Canvas._currentColorMap.find("original",hexColor);
            this.form.setValues({
                "inputColor":this.inputColor,
                "Hex":hexColor,
                "changeTo":skinColor?skinColor.current:hexColor,
                "Name":c.toName(),
                "Rgb":c.toRgbString(),
                "Hsl":c.toHslString(),
                "Hwb":c.toHwbString(),
                "Cmyk":c.toCmykString()
            });
        }else{
            this.form.setValues({"inputColor":this.inputColor});
        }
    }
);
isc.B._maxIndex=isc.C+4;

isc.defineClass("SkinPaletteTools","HLayout");
isc.A=isc.SkinPaletteTools.getPrototype();
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.membersMargin=5;
isc.A.layoutMargin=5;
isc.A.leftPaneDefaults={
        _constructor:"VLayout",
        membersMargin:5
    };
isc.A.gridDefaults={
        _constructor:"ListGrid",
        height:400,
        headerHeight:56,
        dataSource:"colorSearchDS",
        autoFetchData:true,
        autoFitData:"horizontal",
        autoFitFieldWidths:true,
        autoFitWidthApproach:"both",
        getCellCSSText:function(record,rowNum,colNum){
            return"background-color: "+record.original+";"
        },
        canHover:true,
        showHover:true,
        cellHoverHTML:function(record,rowNum,colNum,defaultValue){
            if(colNum>2)return record[this.creator.colorArrays[colNum-3]].join("\n");
            return defaultValue;
        },
        fields:[
            {name:"original",title:"Original",width:100},
            {name:"isSkinCSS",type:"boolean",title:"From Skin"},
            {name:"mostlyType",editorType:"SelectItem",valueMap:["","Backgrounds","Fonts","Borders"]},
            {name:"backgroundCount",title:"Background"},
            {name:"colorCount",title:"Text"},
            {name:"borderCount",title:"Border"},
            {name:"borderTopCount",title:"Top"},
            {name:"borderLeftCount",title:"Left"},
            {name:"borderBottomCount",title:"Bottom"},
            {name:"borderRightCount",title:"Right"},
            {name:"background",hidden:true},
            {name:"color",hidden:true},
            {name:"border",hidden:true},
            {name:"borderTop",hidden:true},
            {name:"borderLeft",hidden:true},
            {name:"borderBottom",hidden:true},
            {name:"borderRight",hidden:true},
            {name:"red",type:"integer",hidden:true},
            {name:"green",type:"integer",hidden:true},
            {name:"blue",type:"integer",hidden:true},
            {"name":"lab",hidden:true}
        ],
        headerSpans:[
            {
                fields:["original","isSkinCSS","mostlyType","backgroundCount","colorCount"],
                title:"Color"
            },
            {
                fields:["borderCount","borderTopCount","borderLeftCount","borderBottomCount","borderRightCount"],
                title:"Borders"
            }
        ],
        recordClick:function(viewer,record,recordNum,field,fieldNum,value,rawValue){
            viewer.creator.converter.setColor(record.original);
        },
        showFilterEditor:true,
        canGroupby:true,
        canMultiGroup:true,
        allowFilterExpressions:true,
        dataArrived:function(){
            this.creator.rowCountLabel.setContents(this.data.getLength()+" filtered rows...");
        },
        initialCriteria:{_constructor:"AdvancedCriteria",operator:"and",
            criteria:[
                {fieldName:"isSkinCSS",operator:"equals",value:true}
            ]
        },
        initialSort:[{property:"lab",direction:"ascending"}]
    };
isc.A.buttonBarDefaults={
        _constructor:"HLayout",
        overflow:"visible",
        membersMargin:5,
        height:40
    };
isc.A.loadConfigButtonDefaults={
        _constructor:"IButton",
        title:"Load Config",
        autoFit:true,
        click:function(){
            this.creator.loadConfig();
        }
    };
isc.A.saveConfigButtonDefaults={
        _constructor:"IButton",
        title:"Save Config",
        autoFit:true,
        click:function(){
            this.creator.saveConfig();
        }
    };
isc.A.allButtonDefaults={
        _constructor:"IButton",
        title:"All",
        autoFit:true,
        click:function(){
            this.creator.grid.setCriteria({});
        }
    };
isc.A.only1RefButtonDefaults={
        _constructor:"IButton",
        title:"Only 1 ref",
        autoFit:true,
        click:function(){
            this.creator.grid.setCriteria(this.creator.getSearchCrit("lessThan",2));
        }
    };
isc.A.lessThan5RefsButtonDefaults={
        _constructor:"IButton",
        title:"Less than 5 refs",
        autoFit:true,
        click:function(){
            this.creator.grid.setCriteria(this.creator.getSearchCrit("lessThan",5));
        }
    };
isc.A.rowCountLabelDefaults={
        _constructor:"Label",
        autoFit:true,
        wrap:false,
        color:"white"
    };
isc.A.rightPaneDefaults={
        _constructor:"VLayout",
        membersMargin:5,
        height:"100%"
    };
isc.A.paletteDefaults={
        _constructor:"SkinColorPalette",
        title:"Palette Colors"
    };
isc.A.closestPaletteDefaults={
        _constructor:"SkinColorPalette",
        title:"Closest Colors"
    };
isc.A.converterDefaults={
        _constructor:"ColorConverter",
        setColor:function(color){
            this.Super("setColor",arguments);
            this.creator.converterColorChanged(this.inputColor);
        }
    };
isc.A.repo=null;
isc.B.push(isc.A.initWidget=function isc_SkinPaletteTools_initWidget(){
        this.Super("initWidget",arguments);
        this.colorArrays=isc.SkinUtil.colorArrays;
        this.config={colors:[],fonts:[]};
        this.createDataSources();
        this.addAutoChild("leftPane");
        this.addMember(this.leftPane);
        this.addAutoChild("grid");
        this.leftPane.addMember(this.grid);
        this.addAutoChild("buttonBar");
        this.leftPane.addMember(this.buttonBar,0);
        this.addAutoChildren(["loadConfigButton","saveConfigButton","allButton","only1RefButton","lessThan5RefsButton","rowCountLabel"]);
        this.buttonBar.addMembers([this.loadConfigButton,this.saveConfigButton,this.allButton,this.only1RefButton,this.lessThan5RefsButton,this.rowCountLabel])
        this.addAutoChild("rightPane")
        this.addMember(this.rightPane);
        this.addAutoChild("converter");
        this.rightPane.addMember(this.converter);
        this.addAutoChild("palette",{title:"Palette Colors"});
        this.rightPane.addMember(this.palette);
        this.addAutoChild("closestPalette",{title:"Closest Colors",colors:[]});
        this.rightPane.addMember(this.closestPalette);
    }
,isc.A.converterColorChanged=function isc_SkinPaletteTools_converterColorChanged(inputColor){
        this.palette.selectColor(inputColor);
        this.closestPalette.drawColors(isc.SkinUtil.getClosestColors(inputColor,50));
    }
,isc.A.getSearchCrit=function isc_SkinPaletteTools_getSearchCrit(operator,value){
        var crit={_constructor:"AdvancedCriteria",operator:"and",criteria:[]};
        for(var i=0;i<this.colorArrays.length;i++){
            crit.criteria.add({fieldName:this.colorArrays[i]+"Count",operator:operator,value:value});
        }
        return crit
    }
,isc.A.createDataSources=function isc_SkinPaletteTools_createDataSources(){
        isc.SkinUtil.createDataSources();
        this.colorSearchDS=isc.SkinUtil.colorSearchDS;
    }
,isc.A.getRepo=function isc_SkinPaletteTools_getRepo(){
        if(!this.repo)this.repo=isc.ViewRepo.create();
        return this.repo;
    }
,isc.A.loadConfig=function isc_SkinPaletteTools_loadConfig(callback,force){
        var _callback=callback;
        if(!force&&this.configChanged){
            isc.ask("Config has been changed - proceed anyway (changes will be lost)?",
                function(value){
                    if(value!=null&&value==true){
                        this.loadConfig(_callback,true);
                    }
                }
            );
            return;
        }
        var _this=this;
        this.loadFile("/isomorphic/skins/_internalSource/tools/skinColors/skin.config",
            function(data){
                _this.configLoaded(data,_callback);
            }
        );
    }
,isc.A.configLoaded=function isc_SkinPaletteTools_configLoaded(data,callback){
        this.config=isc.JSON.decode(data);
        isc.logWarn(isc.echoFull(data));
        var colorCount=this.config.colors.length,
            colorMap={};
        if(colorCount>0){
            for(var i=0;i<colorCount;i++){
                var entry=this.config.colors[i];
                colorMap[entry.original]=entry.current;
            }
            isc.Canvas.changeSkinColors(colorMap);
        }
        if(callback)this.fireCallback(callback,"data",[data]);
    }
,isc.A.loadFile=function isc_SkinPaletteTools_loadFile(path,callback){
        var repo=this.getRepo();
        var _callback=callback;
        repo.loadObject({criteria:{path:path}},
            function(data,context,callback){
                _callback(data);
            }
        );
    }
,isc.A.saveFile=function isc_SkinPaletteTools_saveFile(contents,path,callback){
        var repo=this.getRepo();
        var _callback=callback;
        var _this=this;
        var ds=isc.DS.get("Filesystem");
        var record={
            path:path,
            name:path,
            fileName:path,
            contents:isc.JSON.encode(contents)
        };
        ds.updateData(record,
            function(dsResponse){
                _this.saveObjectReply(dsResponse,_callback,record);
            }
        );
    }
,isc.A.saveObjectReply=function isc_SkinPaletteTools_saveObjectReply(dsResponse,callback,context){
        this.configChanged=false;
        if(callback)this.fireCallback(callback,"success",[true]);
    }
,isc.A.saveConfig=function isc_SkinPaletteTools_saveConfig(config,callback){
        config=config||this.config
        this.saveFile(config,"/isomorphic/skins/_internalSource/tools/skinColors/skin.config",
            function(success){
                isc.logWarn(success);
            }
        );
    }
,isc.A.changeConfigColor=function isc_SkinPaletteTools_changeConfigColor(oldColor,newColor,types){
        var entry=this.config.colors.find("original",oldColor);
        if(entry){
            entry.current=newColor;
        }else{
            if(!types)types={background:true,border:true,color:true}
            entry={original:oldColor,current:newColor,types:types};
            this.config.colors.add(entry);
        }
        this.configChanged=true;
    }
);
isc.B._maxIndex=isc.C+12;

isc.showPaletteTools=isc.SkinUtil.showSkinPaletteTools;
isc.defineClass("SkinFunc");
isc.A=isc.SkinFunc;
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.repo=null;
isc.A.seriesSassPath="/isomorphic/skins/_internalSource/FlatSeries/sass/";
isc.A.skinsFolderPath="/isomorphic/skins/";
isc.A.currentSeries=null;
isc.A.shouldImportSkinVars=false;
isc.A.transformTypes={
        "none":"None",
        "lighten":"Lighten",
        "darken":"Darken",
        "saturate":"Saturate",
        "desaturate":"Desaturate",
        "custom":"Custom"
    };
isc.B.push(isc.A.loadFile=function isc_c_SkinFunc_loadFile(path,callback){
        if(!path){
            isc.say("loadFile called with no file-path");
            return null;
        }
        var _callback=callback;
        if(!isc.SkinFunc.repo){
            isc.SkinFunc.repo=isc.ViewRepo.create({
                dataSource:isc.DS.get("Filesystem")
            });
        }
        isc.SkinFunc.repo.loadObject({criteria:{path:path}},
            function(data,context,callback){
                _callback(data);
            }
        );
        return;
    }
,isc.A.saveFile=function isc_c_SkinFunc_saveFile(contents,path,callback){
        if(!path){
            isc.say("saveFile called with no file-path");
            return null;
        }
        var _callback=callback;
        var content=(isc.isA.String(contents)?contents:
                isc.JSON.encode(contents,{prettyPrint:false}));
        var ds=isc.DS.get("Filesystem");
        var record={
            path:path,
            name:path,
            fileName:path,
            contents:content
        };
        ds.updateData(record,
            function(dsResponse){
                if(_callback)_callback(record);
            }
        );
    }
,isc.A.init=function isc_c_SkinFunc_init(){
        var SF=isc.SkinFunc;
        SF.createDataSources();
    }
,isc.A.createDataSources=function isc_c_SkinFunc_createDataSources(){
        isc.SkinFunc.storeVariableGroups();
        var SF=isc.SkinFunc;
        SF.skinOutputGroupsDS=isc.DataSource.create({
            ID:"skinOutputGroupsDS",
            clientOnly:true,
            fields:[
                {name:"name",type:"text",primaryKey:true},
                {name:"parentId",type:"text"},
                {name:"index",type:"integer"},
                {name:"title",type:"text"},
                {name:"comment",type:"text"}
            ],
            cacheData:[
                {index:0,name:"global",title:"Global Settings",comment:"Flags and other settings that have skin-wide effects"},
                {index:10,name:"font",title:"Fonts",comment:"Variables for fonts"},
                {index:15,name:"colors",title:"Standard Colors",comment:"Variables for Colors"},
                {index:20,name:"standard",title:"Standard Settings",comment:"Base settings that are derived by more localized variables"},
                {index:40,name:"button",title:"Buttons",comment:"Variables for Button styles"},
                {index:45,name:"window",title:"Windows and Dialogs",comment:"Variables used for styling Windows and Dialogs"},
                {index:50,name:"menu",title:"Menus and MenuButtons",comment:"Variables for styling Menus and MenuButtons"},
                {index:60,name:"tab",title:"Tabs",comment:"Variables for Tab styles"},
                {index:70,name:"headerButton",title:"HeaderButtons",comment:"Variables for HeaderButton styles"},
                {index:80,name:"sectionStack",title:"SectionStacks",comment:"Variables for SectionStack styles"},
                {index:90,name:"navigationBar",title:"Navigation Bar",comment:"Variables for Navigation Bar styles"},
                {index:100,name:"listGrid",title:"ListGrids",comment:"Variables for ListGrid styles"},
                {index:105,name:"treeGrid",title:"Trees",comment:"Variables for TreeGrid styles"},
                {index:110,name:"cube",title:"CubeGrid",comment:"Variables for CubeGrid styles"},
                {index:115,name:"pickList",title:"PickLists",comment:"Variables for PickList styles"},
                {index:120,name:"tileGrid",title:"TileGrids",comment:"Variables for TileGrid styles"},
                {index:130,name:"splitBar",title:"SplitBars",comment:"Variables used for styling SplitBars"},
                {index:140,name:"richTextEditor",title:"RichTextEditors",comment:"Variables used for styling RichTextEditors"},
                {index:150,name:"toolStrip",title:"ToolStrips",comment:"Variables used for styling ToolStrips"},
                {index:160,name:"ribbon",title:"Ribbons and IconButtons",comment:"Variables used for styling Ribbons and IconButtons"},
                {index:170,name:"scrollBar",title:"Scrollbars",comment:"Variables used for styling Scrollbars"},
                {index:180,name:"form",title:"DynamicForms and FormItems",comment:"Variables used for styling DynamicForms and the various FormItem types"},
                {index:185,name:"detailViewer",title:"DetailViewers",comment:"Variables used for styling DetailViewers"},
                {index:190,name:"slider",title:"Sliders",comment:"Variables used for styling Sliders"},
                {index:200,name:"dateChooser",title:"DateChoosers",comment:"Variables used for styling DateChoosers"},
                {index:210,name:"colorPicker",title:"ColorPickers",comment:"Variables used for styling ColorPickers"},
                {index:220,name:"calendar",title:"Calendars",comment:"Variables used for styling Calendars"},
                {index:300,name:"filterBuilder",title:"FilterBuilder",comment:"Settings that affect FilterBuilder widgets"},
                {index:310,name:"charts",title:"Charting",comment:"Settings that affect Charts"},
                {index:320,name:"hoverCanvas",title:"Hover Canvas",comment:"Settings that affect the hover canvas"},
                {index:990,name:"misc",title:"Miscellaneous",comment:"Miscellaneous variables"},
                {index:1990,name:"showcase",title:"Showcase",comment:"Styles for the Isomorphic Showcases"}
            ]
        });
        SF.skinVariableDS=isc.DataSource.create({
            ID:"skinVariablesDS",
            clientOnly:true,
            fields:[
                {name:"id",type:"integer",primaryKey:true,width:80},
                {name:"name",type:"text",title:"Name"},
                {name:"usageType",type:"text",title:"Usage Type"},
                {name:"superKey",type:"text",title:"Super Key"},
                {name:"derivesFrom",type:"text",title:"Derives From"},
                {name:"transform",type:"text",title:"Transform"},
                {name:"category",type:"text",title:"Category"},
                {name:"value",type:"text",title:"Value"},
                {name:"defaultValue",type:"text",title:"Default Value"},
                {name:"valueType",type:"text",width:80,title:"Value Type"},
                {name:"valueSubType",type:"text",width:80,title:"Value Sub-type"},
                {name:"usedBy",type:"text",multiple:true,title:"Used By"},
                {name:"keywords",type:"text",multiple:true,title:"Keywords"},
                {name:"usedInSeriesFile",type:"boolean",hidden:true,title:"Used In Series"},
                {name:"usedInThemeFile",type:"boolean",hidden:true,title:"Used in Theme"},
                {name:"exclude",type:"boolean",title:"Exclude"},
                {name:"flagAsImportant",type:"boolean",title:"Important"},
                {name:"outputIndex",type:"integer",title:"Output Index"},
                {name:"outputGroup",type:"text",title:"Group"}
            ]
        });
    }
,isc.A.getVariablesDS=function isc_c_SkinFunc_getVariablesDS(dsName,data){
        var ds=isc.DataSource.create({
            ID:dsName,
            clientOnly:true,
            inheritsFrom:"skinVariables"
        });
        if(data){
            try{
                if(isc.isA.String(data)){
                    var d=eval(data);
                    data=d;
                }
            }catch(e){
            }
            ds.cacheData=data;
        }
        return ds;
    }
,isc.A.editVariableName=function isc_c_SkinFunc_editVariableName(attr,series,dbc){
        series=series||isc.SkinFunc.currentSeries;
        var attrName=(isc.isAn.Object(attr)?attr.name:attr);
        if(attrName.startsWith("$"))attrName=attrName.substring(1);
        isc.askForValue("Rename '"+attrName+"' to:",function(value){
            if(!value){
                isc.say("No value entered");
                return;
            }
            value=value.trim();
            if(!value.startsWith("$"))value="$"+value;
            isc.SkinFunc.changeVariableName(series,attr,value);
            if(dbc)dbc.invalidateCache();
        },{defaultValue:attrName,width:400});
    }
,isc.A.changeVariableName=function isc_c_SkinFunc_changeVariableName(series,fromName,toName){
        series.changeVariableName(fromName,toName);
    }
,isc.A.getThemeOutputPath=function isc_c_SkinFunc_getThemeOutputPath(themeName,local){
        if(local)return"/tools/skinTools/variableEditor/";
        return isc.SkinFunc.skinsFolderPath+themeName+"/_internalSource/sass/";
    }
,isc.A.getOutputPath=function isc_c_SkinFunc_getOutputPath(local){
        if(local)return"/tools/skinTools/variableEditor/";
        return isc.SkinFunc.seriesSassPath;
    }
,isc.A.loadSeries=function isc_c_SkinFunc_loadSeries(seriesName,themeNames,callback){
        var series=isc.SkinSeries.create({
            ID:"series_"+seriesName,
            seriesName:seriesName,
            themeNames:themeNames,
            _loadCallback:callback
        });
        series.loadSeries(seriesName,themeNames);
    }
,isc.A.readSection=function isc_c_SkinFunc_readSection(keyName,data){
        if(!data)return"";
        var startTag="//>"+keyName,
            start=data.indexOf(startTag)+startTag.length,
            end=data.indexOf("//<"+keyName)
        ;
        return data.substring(start,end);
    }
,isc.A.getSectionContent=function isc_c_SkinFunc_getSectionContent(keyName,data){
        data=data||"";
        var result="//>"+keyName+"\n"+data.trim()+"\n//<"+keyName+"\n";
        isc.logWarn("writing section "+keyName+"\n\n"+result);
        return result;
    }
,isc.A.remapOutputIndices=function isc_c_SkinFunc_remapOutputIndices(){
        var groups=isc.SkinFunc.variableGroupCache;
        var topLevel=[];
        for(var i=0;i<groups.length;i++){
            if(groups[i].parentId==null)topLevel.add(groups[i]);
        }
        topLevel.setSort([{property:"index",direction:"ascending"}]);
        skinVariables.fetchData({},function(response,data){
            for(var i=0;i<topLevel.length;i++){
                var topGroup=topLevel[i];
                var kids=groups.findAll("parentId",topGroup.name)||[];
                kids.setSort([{property:"index",direction:"ascending"}]);
                if(kids.length>0){
                    for(var j=0;j<kids.length;j++){
                        var vars=data.findAll({outputGroup:kids[j].name})||[];
                        vars.setSort([{property:"outputIndex",direction:"ascending"}]);
                        for(var i=0;i<vars.getLength();i++){
                            var attr=vars.get(i);
                            attr.outputIndex=i*10;
                            skinVariables.updateData(attr);
                        }
                    }
                }
            }
        });
    }
,isc.A.storeVariableGroups=function isc_c_SkinFunc_storeVariableGroups(){
        skinVariableGroups.fetchData({},function(response,data){
            isc.SkinFunc.variableGroupCache=data;
        });
    }
,isc.A.getVariableScript=function isc_c_SkinFunc_getVariableScript(passedData,props){
        var _data=(isc.isA.ResultSet(passedData)?passedData.allRows:passedData),
            results=[]
        ;
        var groups=isc.SkinFunc.variableGroupCache;
        var topLevel=[];
        for(var i=0;i<groups.length;i++){
            if(groups[i].parentId==null)topLevel.add(groups[i]);
        }
        topLevel.setSort([{property:"index",direction:"ascending"}]);
        for(var i=0;i<topLevel.length;i++){
            var topGroup=topLevel[i];
            var kids=groups.findAll("parentId",topGroup.name)||[];
            kids.setSort([{property:"index",direction:"ascending"}]);
            if(kids.length>0){
                results.add("\n// ==== "+topGroup.title+(topGroup.comment?" - "+topGroup.comment:""));
                for(var j=0;j<kids.length;j++){
                    var data=_data.findAll({outputGroup:kids[j].name})||[];
                    data.setSort([{property:"outputIndex",direction:"ascending"}]);
                    if(data.length>0){
                        if(kids[j].comment)results.add("// "+kids[j].comment);
                        else if(kids[j].title){
                            results.add("// "+kids[j].title);
                        }
                        for(var k=0;k<data.getLength();k++){
                            var attr=data.get(k);
                            if(!props.includeAll&&attr.exclude)continue;
                            var value=null;
                            if(props.exportFieldName){
                                if(attr[props.exportFieldName])value=attr[props.exportFieldName];
                            }
                            if(value==null)value=(props.returnDefaults?attr.defaultValue:attr.value);
                            if(value==null)value=attr.value;
                            value=value.replaceAll("!default").trim();
                            if(value.endsWith(";"))value=value.substring(0,value.length-1).trim();
                            var result=attr.name+": "+value;
                            if(attr.flagAsImportant)result+=" !important";
                            if(props.returnDefaults)result+=" !default";
                            if(!result.endsWith(";"))result+=";";
                            results.add(result);
                        }
                    }
                }
            }
        }
        var results=results.join("\n");
        return results;
    }
,isc.A.getExcludedRecords=function isc_c_SkinFunc_getExcludedRecords(data){
        data=data||isc.SkinFunc.currentSeries.variablesRS.allRows;
        var result=",",
            excludedCount=0
        ;
        for(var i=0;i<data.length;i++){
            if(data[i].exclude){
                excludedCount++;
                result+=","+data[i].name;
            }
        }
        result=result.replaceAll(",,","");
        isc.logWarn("Returning "+excludedCount+" excluded skin-attributes:\n\n"+isc.echoFull(result));
    }
,isc.A.importSkinVars=function isc_c_SkinFunc_importSkinVars(theme){
        var list=[],
            attrStart=-1,
            attrEnd,
            lineEnd=-1,
            lastVar,
            lastValue
        ;
        var SF=isc.SkinFunc,
            counter=0
        ;
        var vars=theme.themeVariableData;
        while(2>1){
            attrStart=vars.indexOf("$",lineEnd+1);
            if(attrStart==-1){
                break;
            }
            attrEnd=vars.indexOf(":",attrStart);
            lineEnd=vars.indexOf(";",attrEnd+1)
            var newAttr={id:counter++,
                name:lastVar,
                value:lastValue,
                usedBy:[],
                name:vars.substring(attrStart,attrEnd),
                defaultValue:vars.substring(attrEnd+1,lineEnd).replaceAll("\n").trim()
            };
            if(newAttr.defaultValue.contains("!important")){
                newAttr.flagAsImportant=true;
                newAttr.defaultValue=newAttr.replaceAll("!important");
            }else{
                newAttr.flagAsImportant=false;
            }
            newAttr.defaultValue=newAttr.defaultValue.trim();
            newAttr.value=newAttr.defaultValue;
            newAttr.keywords=newAttr.name.substring(1).split("_");
            newAttr.usageType=newAttr.keywords?newAttr.keywords[0]:"none";
            list.add(newAttr);
        }
        var series=SF.currentSeries,
            base=series.baseFileContent,
            themeData=series.themes[0].themeVariableData
        ;
        for(var i=0;i<list.length;i++){
            var attr=list[i];
            for(var j=0;j<list.length;j++){
                if(list[j].value==attr.name){
                    list[j].superKey=!list[j].superKey?"":list[j].superKey+":";
                    list[j].superKey+=attr.name;
                }else if(list[j].value.contains(attr.name)){
                    attr.usedBy.add(list[j].name);
                }
            }
            if(base.contains(attr.name+";")||
                base.contains(attr.name+",")||
                base.contains(attr.name+" ")||
                base.contains(attr.name+")")
            ){
                attr.usedInSeriesFile=true;
            }else attr.usedInSeriesFile=false;
            if(themeData.contains(attr.name+";")||
                themeData.contains(attr.name+",")||
                themeData.contains(attr.name+" ")||
                themeData.contains(attr.name+")")
            ){
                attr.usedInThemeFile=true;
            }else attr.usedInThemeFile=false;
            if(!attr.usedInSeriesFile&&!attr.usedInThemeFile){
                attr.exclude=(attr.name.endsWith("_hue")||
                    attr.name.endsWith("_saturation")||
                    attr.name.endsWith("_lightness"));
            }else{
                attr.exclude=false;
            }
            if(attr.category)continue;
            if(attr.name.endsWith("_bgColor")){
                attr.category="color";
                attr.valueType="color";
                attr.valueSubType="background";
            }else if(attr.name.endsWith("_border_color")){
                attr.category="color";
                attr.valueType="color";
                attr.valueSubType="border";
            }else if(attr.name.endsWith("_color")){
                attr.category="color";
                attr.valueType="color";
                attr.valueSubType="text";
            }else if(attr.name.endsWith("_hue")){
                attr.category="color";
                attr.valueType="color";
                attr.valueSubType="hue";
            }else if(attr.name.endsWith("_saturation")){
                attr.category="color";
                attr.valueType="color";
                attr.valueSubType="saturation";
            }else if(attr.name.endsWith("_lightness")){
                attr.category="color";
                attr.valueType="color";
                attr.valueSubType="lightness";
            }else if(attr.name.contains("font_size")){
                attr.category="font";
                attr.valueType="font";
                attr.valueSubType="size";
            }else if(attr.name.contains("font")){
                attr.category="font";
                attr.valueType="font";
                attr.valueSubType="font";
            }
        }
        return list;
    }
,isc.A.transformColor=function isc_c_SkinFunc_transformColor(color,transform,delta){
        var SU=isc.SkinUtil;
        color=SU.getSkinColor(color);
        if(transform=="custom"){
        }else if(transform!="none"){
            if(color[transform])color[transform](delta);
        }
        return SU.toHexString(color);
    }
,isc.A.updateVariableMetaData=function isc_c_SkinFunc_updateVariableMetaData(series,transforms){
        var ds=skinVariables,
            data=series.variablesRS.allRows,
            base=series.baseFileContent
        ;
        var excludeCountPos=0,
            excludeCountNeg=0
        ;
        transforms=transforms||{};
        if(transforms.updateSuperKeys==null)transforms.updateSuperKeys=false;
        if(transforms.updateUsageAndExclude==null)transforms.updateUsageAndExclude=false;
        if(transforms.removeUsedBy==null)transforms.removeUsedBy=true;
        for(var i=0;i<data.length;i++){
            var record=isc.addProperties({},data[i]);
            if(transforms.updateSuperKeys){
                delete record.derivesFrom;
                delete record.transform;
                if(record.superKey==null){
                    if(record.value&&record.value.startsWith("$")){
                        if(!record.value.contains(" ")&&record.value.contains(",")){
                            record.superKey=record.value;
                        }
                    }
                }else if(record.superKey.contains(" ")||record.superKey.contains(",")){
                    delete record.superKey;
                }
                if(record.superKey&&!record.derivesFrom){
                    record.derivesFrom=record.superKey;
                    record.transform="none";
                }
            }
            if(transforms.updateUsageAndExclude){
                var wasUsedInSeriesFile=record.usedInSeriesFile,
                    wasUsedInThemeFile=record.usedInThemeFile
                ;
                if(base.contains(record.name+";")||
                    base.contains(record.name+",")||
                    base.contains(record.name+" ")||
                    base.contains(record.name+")")
                ){
                    record.usedInSeriesFile=true;
                }else record.usedInSeriesFile=false;
                var found=false;
                for(var k=0;k<series.themes.length;k++){
                    var themeVars=series.themes[k].themeVariableData;
                    if(themeVars.contains(record.name+";")||
                        themeVars.contains(record.name+",")||
                        themeVars.contains(record.name+" ")||
                        themeVars.contains(record.name+")")
                    ){
                        found=true;
                        break;
                    }
                    var themeCustom=series.themes[k].customFileContent;
                    if(themeCustom.contains(record.name+";")||
                        themeCustom.contains(record.name+",")||
                        themeCustom.contains(record.name+" ")||
                        themeCustom.contains(record.name+")")
                    ){
                        found=true;
                        break;
                    }
                }
                record.usedInThemeFile=found;
                if(wasUsedInSeriesFile!=record.usedInSeriesFile||wasUsedInThemeFile!=record.usedInThemeFile){
                }
                if(!record.usedInSeriesFile&&!record.usedInThemeFile){
                    excludeCountPos++
                    if(!record.exclude)isc.logWarn("newly excluding record... "+record.name);
                    record.exclude=true;
                }else if(record.usedInSeriesFile||record.usedInThemeFile){
                    excludeCountNeg++;
                    if(record.exclude)isc.logWarn("newly including record... "+record.name);
                    record.exclude=false;
                }
                continue;
                if(!record.usedInSeriesFile&&!record.usedInThemeFile){
                    record.exclude=(record.name.endsWith("_hue")||
                        record.name.endsWith("_saturation")||
                        record.name.endsWith("_lightness"));
                }else{
                    record.exclude=false;
                }
            }
            if(transforms.removeUsedBy){
                delete record.usedBy;
            }
            ds.updateData(record);
        }
    }
,isc.A.exportToNewDSFormat=function isc_c_SkinFunc_exportToNewDSFormat(ds){
        var data=ds.cacheData,
            newDS=skinVariables,
            recordCount=0
        ;
        data.setSort({property:"id",direction:"ascending"});
        for(var i=0;i<data.length;i++){
            var item=isc.addProperties({},data[i]);
            if(item.exclude==true)continue;
            if(item.exclude==null)item.exclude=false;
            if(item.superKey==item.derivesFrom)delete item.superKey;
            delete item.usedInThemeFile;
            delete item.usedBy;
            item.id=++recordCount;
            newDS.addData(item);
        }
    }
);
isc.B._maxIndex=isc.C+20;

isc.defineClass("SkinVariable");
isc.A=isc.SkinVariable;
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.B.push(isc.A.fromRecordArray=function isc_c_SkinVariable_fromRecordArray(records){
        var a=[];
        for(var i=0;i<records.length;i++){
            a.add(isc.SkinVariable.create(records[i]));
        }
        return a;
    }
);
isc.B._maxIndex=isc.C+1;

isc.A=isc.SkinVariable.getPrototype();
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.id=null;
isc.A.name=null;
isc.A.usageType=null;
isc.A.superKey=null;
isc.A.category=null;
isc.A.value=null;
isc.A.defaultValue=null;
isc.A.valueType=null;
isc.A.valueSubType=null;
isc.A.usedBy=null;
isc.A.keywords=null;
isc.A.usedInSeriesFile=null;
isc.A.usedInThemeFile=null;
isc.A.exclude=null;
isc.A.flagAsImportant=null;
isc.B.push(isc.A.init=function isc_SkinVariable_init(){
        this.Super("init",arguments);
        if(!this.usedBy)this.usedBy=[];
        if(!this.keywords)this.keywords=[];
        if(this.record){
            this.fromRecord(this.record);
        }
    }
,isc.A.fromRecord=function isc_SkinVariable_fromRecord(record){
        isc.addProperties(this,record);
    }
,isc.A.getValue=function isc_SkinVariable_getValue(){
        if(this.derivesFrom){
            var SF=isc.SkinFunc,
                vars=SF.currentSeries.variablesRS.allRows,
                parentRecord=vars.find("name",this.superKey),
                parent=parentRecord?isc.SkinVariable.create({record:parentRecord}):null
            ;
            if(parent)return parent.getValue();
        }
        return this.value;
    }
);
isc.B._maxIndex=isc.C+3;

isc.defineClass("SkinSeries");
isc.A=isc.SkinSeries.getPrototype();
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.seriesName="Flat";
isc.A.seriesFilePath="_baseFlat.scss";
isc.A.baseFileContent=null;
isc.A.seriesDefaultVariableData=null;
isc.A.seriesHeaderDocData=null;
isc.A.seriesBaseStylesData=null;
isc.A.seriesMetadataPath="_Flat_SeriesMetadata.scss";
isc.A.seriesMetadataContent=null;
isc.A.generatorProps={
    };
isc.B.push(isc.A.init=function isc_SkinSeries_init(){
        this.Super("init",arguments);
    }
,isc.A.loadSeries=function isc_SkinSeries_loadSeries(seriesName,themeNames,callback){
        isc.logWarn("Loading '"+this.seriesName+"' Series...");
        var SF=isc.SkinFunc,
            _callback=callback,
            _series=this
        ;
        seriesName=seriesName||this.seriesName;
        themeNames=themeNames||this.themeNames;
        _series.seriesName=seriesName;
        _series.seriesFilePath=SF.getOutputPath()+"_base"+_series.seriesName+".scss";
        _series.seriesMetadataPath=SF.getOutputPath(true)+"_"+_series.seriesName+"_SeriesMetadata.js";
        if(themeNames&&!isc.isAn.Array(themeNames))themeNames=[themeNames];
        SF.currentSeries=this;
        skinVariables.fetchData(null,function(response,data,request){
            var rs=isc.ResultSet.create({
                dataSource:skinVariables,
                fetchMode:"local",
                allRows:data
            });
            _series.variablesRS=rs;
            SF.loadFile(_series.seriesMetadataPath,function(data){
                try{
                    var d=eval(data)
                    data=d;
                }catch(e){
                }
                if(isc.isA.String(data))data=isc.JSON.decode(data);
                for(var i=0;i<data.variables.length;i++){
                    if(data.variables[i].outputSubgroupId==null){
                        data.variables[i].outputSubgroupId="standard";
                    }
                }
                _series.seriesMetadataContent=data;
                SF.loadFile(_series.seriesFilePath,function(data){
                    _series.parseSeriesFile(data);
                    if(_series.themeNames&&_series.themeNames.length>0){
                        _series.loadThemes(_series.themeNames,_callback);
                    }else{
                        _series.seriesLoaded(_callback);
                    }
                });
            });
        });
    }
,isc.A.parseSeriesFile=function isc_SkinSeries_parseSeriesFile(data){
        this.baseFileContent=data;
        this.seriesDefaultVariableData=this.readSection("skin_default_variables");
        this.seriesHeaderDocData=this.readSection("skin_header_doc");
        this.seriesBaseStylesData=this.readSection("skin_base_styles");
    }
,isc.A.writeSeriesBaseFile=function isc_SkinSeries_writeSeriesBaseFile(path){
        this._writePath=path;
        this.getDefaultVariableScript(path,{target:this,methodName:"_writeSeriesBaseFile"});
    }
,isc.A._writeSeriesBaseFile=function isc_SkinSeries__writeSeriesBaseFile(variableScript){
        var SF=isc.SkinFunc,
            base=this.baseFileContent
        ;
        var _path=this._writePath||(SF.getOutputPath()+"_base"+this.seriesName+".scss");
        var content=base.substring(0,base.indexOf("//>skin_default_variables"));
        content+=variableScript;
        content+=base.substring(base.indexOf("//<skin_default_variables")+"//<skin_default_variables".length+1,base.length);
        SF.saveFile(content,_path);
    }
,isc.A.writeMetadataFile=function isc_SkinSeries_writeMetadataFile(path){
        return;
        var SF=isc.SkinFunc,
            _series=this
        ;
        var _path=path||(SF.getOutputPath(true)+"_"+this.seriesName+"_SeriesMetadata.js");
        var data=this.variablesRS.allRows;
        this.variablesDS.fetchData(null,
            function(dsResponse,data){
                var result={};
                result.seriesName=_series.seriesName;
                SF.remapOutputIndices(data);
                result.variables=data;
                SF.saveFile(result,_path);
            }
        );
    }
,isc.A.getDefaultVariableScript=function isc_SkinSeries_getDefaultVariableScript(path,callback){
        var _this=this;
        skinVariables.fetchData({},function(response,data){
            var result=isc.SkinFunc.getVariableScript(data,
                isc.addProperties({},_this.generatorProps,{returnDefaults:true})
            );
            result=isc.SkinFunc.getSectionContent("skin_default_variables",result);
            if(callback){
                this.fireCallback(callback,["variableScript"],[result]);
            }
        });
    }
,isc.A.loadThemes=function isc_SkinSeries_loadThemes(themeNames,callback){
        var _callback=callback,
            _series=this
        ;
        this.themes=[];
        this._loadingThemes=isc.shallowClone(themeNames);
        isc.logWarn("Loading '"+this.seriesName+"' Themes...");
        for(var i=0;i<themeNames.length;i++){
            var _themeName=themeNames[i];
            var theme=isc.SkinTheme.create({
                ID:"theme_"+_themeName,
                series:_series,
                themeName:_themeName,
                themeLoaded:function(){
                    _series.themeLoaded(this,_callback)
                }
            });
            this.themes.add(theme);
            theme.loadTheme(_themeName);
        }
    }
,isc.A.themeLoaded=function isc_SkinSeries_themeLoaded(theme,callback){
        isc.logWarn("Theme '"+theme.themeName+"' loaded ("+this.seriesName+" series)");
        this._loadingThemes.remove(theme.themeName);
        if(this._loadingThemes.length==0){
            this.themesLoaded(callback);
        }
    }
,isc.A.themesLoaded=function isc_SkinSeries_themesLoaded(callback){
        isc.logWarn("All '"+this.seriesName+"' series Themes loaded ("+isc.echoFull(this.themes.getProperty("themeName"))+")");
        this.seriesLoaded(callback);
    }
,isc.A.seriesLoaded=function isc_SkinSeries_seriesLoaded(callback){
        isc.logWarn("'"+this.seriesName+"' series loaded...");
        if(this._loadCallback)this._loadCallback(this);
        if(callback)callback(this);
    }
,isc.A.replaceVariableNameInValue=function isc_SkinSeries_replaceVariableNameInValue(fromName,toName,value){
        var result=""+value;
        result=result.replaceAll(fromName+";",toName+";");
        result=result.replaceAll(fromName+",",toName+",");
        result=result.replaceAll(fromName+" ",toName+" ");
        result=result.replaceAll(fromName+")",toName+")");
        return result;
    }
,isc.A.renameVariables=function isc_SkinSeries_renameVariables(fromStr,toStr){
        var data=this.variablesRS.allRows,
            updatedRows=[]
        ;
        if(!fromStr.startsWith("$"))fromStr="$"+fromStr;
        for(var i=0;i<data.length;i++){
            var attr=isc.addProperties({},data[i]);
            if(attr.name.startsWith(fromStr)){
                var fromName=attr.name,
                    toName=attr.name.replace(fromStr,toStr)
                ;
                this.changeVariableName(fromName,toName);
            }
        }
    }
,isc.A.changeVariableName=function isc_SkinSeries_changeVariableName(fromName,toName){
        var data=this.variablesRS.allRows,
            updatedRows=[]
        ;
        for(var i=0;i<data.length;i++){
            var update={};
            var attr=data[i];
            if(attr.name==fromName)update.name=toName;
            if(attr.derivesFrom==fromName)update.derivesFrom=toName;
            if(attr.defaultValue==fromName)update.defaultValue=toName;
            else if(attr.defaultValue&&attr.defaultValue.contains(fromName)){
                update.defaultValue=this.replaceVariableNameInValue(fromName,toName,attr.defaultValue);
            }
            if(attr.value==fromName)update.value=toName;
            else if(attr.value&&attr.value.contains(fromName)){
                update.value=this.replaceVariableNameInValue(fromName,toName,attr.value);
            }
            if(update.name||update.derivesFrom||update.value||update.defaultValue){
                update.id=attr.id;
                updatedRows.add(update);
            }
        }
        for(var i=0;i<updatedRows.length;i++){
            skinVariables.updateData(updatedRows[i]);
        }
        this.baseFileContent=this.replaceVariableNameInValue(fromName,toName,this.baseFileContent);
        this.seriesBaseStylesData=this.replaceVariableNameInValue(fromName,toName,this.seriesBaseStylesData);
        for(var i=0;i<this.themes.length;i++){
            var theme=this.themes[i],
                data=theme.variablesDS.cacheData
            ;
            for(var j=0;j<data.length;j++){
                var attr=data[j];
                if(attr.name==fromName)attr.name=toName;
                if(attr.superKey==fromName)attr.superKey=toName;
                if(attr.derivesFrom==fromName)attr.derivesFrom=toName;
                if(attr.defaultValue==fromName)attr.defaultValue=toName;
                else if(attr.defaultValue&&attr.defaultValue.contains("$")){
                    attr.defaultValue=this.replaceVariableNameInValue(fromName,toName,attr.defaultValue);
                }
                if(attr.value==fromName)attr.value=toName;
                else if(attr.value&&attr.value.contains("$")){
                    attr.value=this.replaceVariableNameInValue(fromName,toName,attr.value);
                }
            }
            theme.customFileContent=this.replaceVariableNameInValue(fromName,toName,theme.customFileContent);
        }
    }
,isc.A.addSeriesVariable=function isc_SkinSeries_addSeriesVariable(variable){
        skinVariables.addData(variable);
    }
,isc.A.readSection=function isc_SkinSeries_readSection(keyName,data){
        data=data||this.baseFileContent;
        return isc.SkinFunc.readSection(keyName,data);
    }
,isc.A.writeAllMetadata=function isc_SkinSeries_writeAllMetadata(){
        this.writeMetadataFile();
        for(var i=0;i<this.themes.length;i++){
            this.themes[i].writeMetadataFile();
        }
    }
,isc.A.writeAllOther=function isc_SkinSeries_writeAllOther(){
        this.writeSeriesBaseFile();
        for(var i=0;i<this.themes.length;i++){
            this.themes[i].writeThemeFile();
        }
    }
);
isc.B._maxIndex=isc.C+18;

isc.defineClass("SkinTheme");
isc.A=isc.SkinTheme.getPrototype();
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.themeName="Tahoe";
isc.A.themeFilePath="_ThemeTahoe.scss";
isc.A.themeFileContent=null;
isc.A.themeFontData=null;
isc.A.themeVariableData=null;
isc.A.themeCustomFilePath="_ThemeTahoe_custom.scss";
isc.A.customFileContent=null;
isc.B.push(isc.A.init=function isc_SkinTheme_init(){
        this.Super("init",arguments);
    }
,isc.A.loadTheme=function isc_SkinTheme_loadTheme(themeName,callback){
        var SF=isc.SkinFunc,
            theme=this
        ;
        theme.themeName=themeName;
        var themePath=SF.getThemeOutputPath(theme.themeName)+"_Theme"+theme.themeName;
        theme.themeFilePath=themePath+".scss";
        theme.themeCustomFilePath=themePath+"_custom.scss";
        var _callback=callback;
        SF.loadFile(theme.themeFilePath,function(data){
            theme.parseThemeFile(data);
            SF.loadFile(theme.themeCustomFilePath,function(data){
                theme.customFileContent=data;
                theme.themeLoaded(_callback);
            });
        });
    }
,isc.A.themeLoaded=function isc_SkinTheme_themeLoaded(callback){
        if(callback)callback();
    }
,isc.A.readSection=function isc_SkinTheme_readSection(keyName,data){
        data=data||this.themeFileContent;
        return isc.SkinFunc.readSection(keyName,data);
    }
,isc.A.parseThemeFile=function isc_SkinTheme_parseThemeFile(data){
        var list=[],
            attrStart=-1,
            attrEnd,
            lineEnd=-1,
            lastVar,
            lastValue
        ;
        this.themeFileContent=data;
        this.themeHeaderContent=this.readSection("theme_header")
        this.themeFontData=this.readSection("theme_fonts");
        this.themeVariableData=this.readSection("theme_variables");
        var vars=this.themeVariableData;
        while(2>1){
            attrStart=vars.indexOf("$",lineEnd+1);
            if(attrStart==-1){
                break;
            }
            attrEnd=vars.indexOf(":",attrStart);
            lineEnd=vars.indexOf(";",attrEnd+1)
            var item={
                name:vars.substring(attrStart,attrEnd),
                value:vars.substring(attrEnd+1,lineEnd).trim()
            };
            list.add(item);
        }
        var baseData=this.series.variablesRS.allRows,
            newData=[]
        ;
        for(var i=0;i<baseData.length;i++){
            var record=isc.addProperties({},baseData[i]),
                lEntry=list.find("name",record.name);
            if(lEntry)record.value=lEntry.value;
            if(record.outputSubgroupId==null)record.outputSubgroupId="standard";
            newData.add(record);
        }
        this.variablesDS=isc.SkinFunc.getVariablesDS(this.themeName+"VariablesDS",newData);
    }
,isc.A.readMetadataFile=function isc_SkinTheme_readMetadataFile(path){
        var SF=isc.SkinFunc,
            _theme=this
        ;
        var _path=path||(SF.getOutputPath(true)+"_"+this.themeName+"_ThemeMetadata.js");
        SF.loadFile(_path,function(data){
            _theme.themeMetadata=data;
            _theme.variablesDS=isc.SkinFunc.getVariablesDS(this.themeName+"VariablesDS",data.variables);
        });
    }
,isc.A.writeMetadataFile=function isc_SkinTheme_writeMetadataFile(path){
        var SF=isc.SkinFunc,
            _theme=this
        ;
        var _path=path||(SF.getOutputPath(true)+"_"+this.themeName+"_ThemeMetadata.js");
        this.variablesDS.fetchData(null,
            function(dsResponse,data){
                var result={};
                result.themeName=_theme.themeName;
                result.variables=data;
                SF.saveFile(result,_path);
            }
        );
    }
,isc.A.writeThemeFile=function isc_SkinTheme_writeThemeFile(path){
        var SF=isc.SkinFunc;
        var _path=path;
        if(!_path){
            _path=SF.getThemeOutputPath(this.themeName)+"_Theme"+this.themeName+".scss";
        }
        var content=SF.getSectionContent("theme_header",this.themeHeaderContent);
        content+=SF.getSectionContent("theme_fonts",this.themeFontData)
        var _this=this;
        this.variablesDS.fetchData(null,
            function(dsResponse,data){
                var result=isc.SkinFunc.getVariableScript(data,
                    isc.addProperties({},_this.generatorProps,{returnDefaults:false})
                );
                content+=SF.getSectionContent("theme_variables",result);
                SF.saveFile(content,_path);
            }
        );
    }
);
isc.B._maxIndex=isc.C+8;

isc.defineClass("SkinVariablesGrid","ListGrid");
isc.A=isc.SkinVariablesGrid.getPrototype();
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A._constructor="ListGrid";
isc.A.width="100%";
isc.A.height="*";
isc.A.showResizesBar=true;
isc.A.autoFetchData=true;
isc.A.dataFetchMode="local";
isc.A.autoSaveEdits=true;
isc.A.canEdit=true;
isc.A.canGroupBy=true;
isc.A.canMultiGroup=true;
isc.A.showFilterEditor=true;
isc.A.filterOnEnter=true;
isc.A.showRowNumbers=true;
isc.A.allowFilterExpressions=true;
isc.A.showClippedValuesOnHover=true;
isc.A.defaultFields=[
        {name:"name",width:"*"},
        {name:"value",width:"*"},
        {name:"category",width:100},
        {name:"valueType",width:120},
        {name:"valueSubType",width:120},
        {name:"flagAsImportant",width:100},
        {name:"outputGroup",width:150,editorProperties:{valueField:"name",displayField:"title"}},
        {name:"outputIndex",title:"Index",width:60},
        {name:"exclude",canToggle:true}
    ];
isc.A.initialCriteria={_constructor:"AdvancedCriteria",operator:"and",
        criteria:[
            {fieldName:"exclude",operator:"equals",value:false}
        ]
    };
isc.B.push(isc.A.recordClick=function isc_SkinVariablesGrid_recordClick(){
        this.creator.variableEditor.editRecord(this.getSelectedRecord());
    }
,isc.A.recordDoubleClick=function isc_SkinVariablesGrid_recordDoubleClick(){
    }
,isc.A.initWidget=function isc_SkinVariablesGrid_initWidget(){
        this.Super("initWidget",arguments);
    }
);
isc.B._maxIndex=isc.C+3;

isc.defineClass("SkinEditor","VLayout");
isc.A=isc.SkinEditor.getPrototype();
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.defaultSeriesName="Flat";
isc.A.defaultSeriesThemeNames=["Tahoe","Stratus","Obsidian","Cascade","FSE"];
isc.A.loadSeriesButtonDefaults={
        _constructor:"IButton",
        title:"Load Series",
        autoFit:true,
        click:function(){
            this.creator.loadSeries();
        }
    };
isc.A.loadSeriesMetadataButtonDefaults={
        _constructor:"IButton",
        title:"Load Series Metadata",
        autoFit:true,
        click:function(){
            this.creator.currentSeries.readMetadataFile();
        }
    };
isc.A.saveSeriesMetadataButtonDefaults={
        _constructor:"IButton",
        title:"Save Series Metadata",
        autoFit:true,
        click:function(){
            this.creator.currentSeries.writeMetadataFile();
        }
    };
isc.A.saveSeriesBaseFileButtonDefaults={
        _constructor:"IButton",
        title:"Save Series SCSS file",
        autoFit:true,
        click:function(){
            this.creator.currentSeries.writeSeriesBaseFile();
        }
    };
isc.A.renameVariableButtonDefaults={
        _constructor:"IButton",
        title:"Rename Variable",
        autoFit:true,
        click:function(){
            var lg=this.creator.variablesGrid,
                record=lg.getSelectedRecord()
            ;
            if(!record)return;
            isc.SkinFunc.editVariableName(record,null,lg);
        }
    };
isc.A.buttonLayoutDefaults={
        _constructor:"HLayout",
        width:"100%",
        height:1,
        layoutMargin:5,
        membersMargin:5
    };
isc.A.bodyLayoutDefaults={
        _constructor:"HLayout",
        width:"100%",
        height:"100%",
        layoutMargin:5,
        membersMargin:5
    };
isc.A.rightLayoutDefaults={
        _constructor:"VLayout",
        width:"100%",
        height:"100%",
        layoutMargin:5,
        membersMargin:5
    };
isc.A.gridButtonLayoutDefaults={
        _constructor:"HLayout",
        width:"100%",
        height:1,
        layoutMargin:5,
        membersMargin:5
    };
isc.A.viewAllButtonDefaults={
        _constructor:"IButton",
        icon:"[SKINIMG]actions/add.png",
        title:"View All",
        autoFit:true,
        autoParent:"gridButtonLayout",
        click:function(){
            var grid=this.creator.variablesGrid;
            grid.clearGroupBy();
            grid.setSort({property:"name",direction:"ascending"});
        }
    };
isc.A.viewForOutputButtonDefaults={
        _constructor:"IButton",
        icon:"[SKINIMG]actions/sort.png",
        title:"View for Output",
        autoFit:true,
        autoParent:"gridButtonLayout",
        click:function(){
            this.creator.gridViewForOutput()
        }
    };
isc.A.skinVariablesGridDefaults={
        _constructor:"SkinVariablesGrid",
        dataSource:"skinVariables"
    };
isc.A.variablesGridDefaults={
        _constructor:"SkinVariablesGrid"
    };
isc.A.variableEditorDefaults={
        _constructor:"SkinVariableEditor",
        width:"100%",
        height:1,
        overflow:"visible"
    };
isc.A.skinTreeDefaults={
        _constructor:"TreeGrid",
        width:300,
        height:"100%",
        showResizeBar:true,
        dataFetchMode:"local",
        fields:[
            {name:"name"}
        ],
        showOpenIcons:false,
        showDropIcons:false,
        showSelectedIcons:true,
        closedIconSuffix:"",
        nodeClick:function(viewer,node){
            this.creator.nodeClicked(node);
        }
    };
isc.B.push(isc.A.nodeClicked=function isc_SkinEditor_nodeClicked(node){
        if(!node)return;
        if(node.id=="styleTester"){
            var url="../colorTester.jsp?skin="+isc.getCurrentSkinName();
            window.open(url);
        }else if(node.id=="paletteTools"){
            isc.showPaletteTools(true);
        }else if(node.id=="stateEditor"){
            var url="./stateEditor.jsp?skin="+isc.getCurrentSkinName();
            window.open(url);
        }
        if(node.hasVariablesDS){
            this.setVariablesGridDataSource(node.variablesDS||node.classObj.variablesDS,node.classObj.variablesRS);
        }
    }
,isc.A.showNodePane=function isc_SkinEditor_showNodePane(node){
        switch(node.id){
            case"seriesMetadata":
                this.setVariablesGridDataSource(this.currentSeries);
            default:
        }
    }
,isc.A.gridViewForOutput=function isc_SkinEditor_gridViewForOutput(){
        var grid=this.variablesGrid;
        grid.setCriteria({exclude:false});
        grid.groupBy(["outputGroup"]);
        grid.setSort([
            {property:"outputIndex",direction:"ascending"}
        ]);
    }
,isc.A.initWidget=function isc_SkinEditor_initWidget(series){
        this.Super("initWidget",arguments);
        this.initRibbon();
        this.addAutoChild("bodyLayout");
        this.addMember(this.bodyLayout);
        this.addAutoChild("skinTree");
        this.bodyLayout.addMember(this.skinTree);
        this.initSkinTree();
        this.addAutoChild("rightLayout");
        this.bodyLayout.addMember(this.rightLayout);
        this.addAutoChild("gridButtonLayout");
        this.rightLayout.addMember(this.gridButtonLayout);
        this.addAutoChildren(["viewAllButton","viewForOutputButton"]);
        this.gridButtonLayout.addMembers([this.viewAllButton,this.viewForOutputButton]);
        this.addAutoChild("variablesGrid",{dataSource:"skinVariables"});
        this.rightLayout.addMember(this.variablesGrid);
        this.addAutoChild("variableEditor",{dataSource:"skinVariables"});
        this.rightLayout.addMember(this.variableEditor);
        this.autoLoadSeries=true;
        if(this.autoLoadSeries){
            this.loadSeries();
        }
    }
,isc.A.initRibbon=function isc_SkinEditor_initRibbon(){
        var getIconButton=function(title,clickFunc,props){
            return isc.IconButton.create(isc.addProperties({
                    title:title,
                    icon:"pieces/16/cube_blue.png",
                    largeIcon:"pieces/48/cube_blue.png",
                    click:clickFunc
                },props)
            );
        };
        var getIconMenuButton=function(title,clickFunc,props){
            return isc.IconMenuButton.create(isc.addProperties({
                    title:title,
                    icon:"pieces/16/piece_blue.png",
                    largeIcon:"pieces/48/piece_blue.png",
                    click:clickFunc
                },props)
            );
        }
        var controls=[];
        if(2==1){
            controls.add(getIconButton("Load Series",
                function(){
                    var seriesName=this.seriesUI.defaultSeriesName,
                        themeNames=this.seriesUI.defaultSeriesThemeNames.duplicate()
                    ;
                    this.seriesUI.loadSeries(seriesName,themeNames);
                },{seriesUI:this}
            ));
            controls.add(getIconButton("Read Metadata",
                function(){
                    this.seriesUI.currentSeries.readAllMetadata();
                },{seriesUI:this}
            ));
        }
        controls.add(getIconButton("Write Metadata",
            function(){
                this.seriesUI.currentSeries.writeAllMetadata();
            },{seriesUI:this}
        ));
        controls.add(getIconButton("Write SCSS Files",
            function(){
                this.seriesUI.currentSeries.writeAllOther();
            },{seriesUI:this}
        ));
        this.seriesRibbonGroup=isc.RibbonGroup.create({
            title:"Series",
            numRows:1,
            colWidths:[40,"*"],
            controls:controls,
            autoDraw:false
        });
        this.ribbonBar=isc.RibbonBar.create({
            groupTitleAlign:"center",
            groupTitleOrientation:"top"
        });
        this.ribbonBar.addGroup(this.seriesRibbonGroup,0);
        this.addMember(this.ribbonBar,0);
    }
,isc.A.initSkinTree=function isc_SkinEditor_initSkinTree(){
        var tg=this.skinTree;
        var ds=isc.DS.get("variablesDS");
        tg.setData(isc.Tree.create({
            modelType:"children",
            nameProperty:"name",
            root:{
                id:"top",name:"Skin Manager",children:[
                    {id:"seriesEditor",name:"Series Editor",children:[]},
                    {id:"utilities",name:"Tools & Utilities",children:[
                        {id:"styleTester",name:"Style Tester",children:[]},
                        {id:"paletteTools",name:"Palette Tools",children:[]},
                        {id:"stateEditor",name:"State Editor",children:[]}
                    ]}
                ]
            }
        }));
        tg.getData().openAll();
    }
,isc.A.loadSeries=function isc_SkinEditor_loadSeries(seriesName,themeNames){
        seriesName=seriesName||this.defaultSeriesName;
        themeNames=themeNames||this.defaultSeriesThemeNames.duplicate();
        var _this=this;
        isc.SkinFunc.loadSeries(seriesName,themeNames,
            function(series){
                _this.seriesLoaded(series);
            }
        );
    }
,isc.A.setVariablesGridDataSource=function isc_SkinEditor_setVariablesGridDataSource(ds,rs){
        this.variablesGrid.setDataSource(ds,this.variablesGrid.getFields());
        this.variablesGrid.setData(rs);
        this.variableEditor.setDataSource(ds);
    }
,isc.A.seriesLoaded=function isc_SkinEditor_seriesLoaded(series){
        this.currentSeries=series;
        var tree=this.skinTree.getData(),
            nSeriesEditor=tree.find("id","seriesEditor")
        ;
        var nSeries=tree.add({id:"_series_"+series.seriesName,name:series.seriesName+" Series",classObj:series},nSeriesEditor);
        var nSeriesMetaData=tree.add({id:series.seriesName+"MetadataFile",name:"Skin Variables",hasVariablesDS:true,classObj:series},nSeries);
        var nSeriesBaseFile=tree.add({id:series.seriesName+"BaseFile",name:"Base SCSS File",classObj:series},nSeries);
        var nThemes=tree.add({id:series.seriesName+"SeriesThemes",name:"Themes",children:[]},nSeries);
        for(var i=0;i<series.themes.length;i++){
            var theme=series.themes[i],
                node={}
            ;
            node.id="_theme_"+theme.themeName;
            node.name=theme.themeName;
            node.hasVariablesDS=true
            node.classObj=theme;
            node.children=[
                {id:"_theme_file_"+node.name,name:"Theme SCSS File",classObj:theme},
                {id:"_theme_variables_"+node.name,name:"Theme Variables",hasVariablesDS:true,classObj:theme},
                {id:"_theme_custom_"+node.name,name:"Custom Styles",classObj:theme},
                {id:"_theme_showcase_"+node.name,name:"Showcase Styles",classObj:theme},
                {id:"_theme_fonts_"+node.name,name:"Theme Fonts",classObj:theme}
            ];
            tree.add(node,nThemes);
        }
        tree.openFolders([nSeriesEditor,nSeries,nThemes]);
        this.skinTree.selectRecord(nSeriesMetaData);
        this.nodeClicked(nSeriesMetaData);
        this.delayCall("gridViewForOutput");
    }
);
isc.B._maxIndex=isc.C+9;

isc.SkinFunc.editSkinVariable=function(record,ds){
    var ed=isc.SkinVariableEditor.create({
        dataSource:ds
    });
    isc.Window.create({
        width:"80%",
        height:"80%",
        backgroundColor:"white",
        title:"Edit Skin Variable",
        children:[
            ed
        ],
        autoCenter:true
    });
    ed.editRecord(record);
}
isc.defineClass("SkinVariableEditor","VLayout");
isc.A=isc.SkinVariableEditor.getPrototype();
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.membersMargin=10;
isc.A.layoutMargin=10;
isc.A.buttonLayoutDefaults={
        _constructor:"HLayout",
        width:"100%",
        height:1,
        membersMargin:10
    };
isc.A.addButtonDefaults={
        _constructor:"IButton",
        icon:"[SKINIMG]actions/add.png",
        title:"Add Variable",
        click:function(){
            this.creator.addRecord();
        }
    };
isc.A.editButtonDefaults={
        _constructor:"IButton",
        icon:"[SKINIMG]actions/edit.png",
        title:"Edit",
        click:function(){
            this.creator.disableUI(false);
        }
    };
isc.A.saveButtonDefaults={
        _constructor:"IButton",
        icon:"[SKINIMG]actions/save.png",
        title:"Save",
        showDisabledIcon:false,
        click:function(){
            this.creator.saveRecord();
        }
    };
isc.A.cancelButtonDefaults={
        _constructor:"IButton",
        icon:"[SKINIMG]actions/cancel.png",
        title:"Cancel",
        showDisabledIcon:false,
        click:function(){
            this.creator.cancelEdit();
        }
    };
isc.A.renameButtonDefaults={
        _constructor:"IButton",
        icon:"[SKINIMG]actions/exclamation.png",
        title:"Rename",
        showDisabledIcon:false,
        click:function(){
            this.creator.renameVariable();
        }
    };
isc.A.editorDefaults={
        _constructor:"DynamicForm",
        width:"100%",
        numCols:8
    };
isc.B.push(isc.A.initWidget=function isc_SkinVariableEditor_initWidget(){
        this.Super("initWidget",arguments);
        this.addAutoChild("buttonLayout");
        this.addMember(this.buttonLayout);
        this.addAutoChildren(["addButton","editButton","saveButton","cancelButton","renameButton"]);
        this.buttonLayout.addMembers([this.addButton,this.editButton,this.saveButton,this.cancelButton,this.renameButton]);
        this.defaultFields=[
            {name:"id",showIf:"false"},
            {name:"name",
                blur:function(){
                    var v=this.getValue();
                    if(v&&v.length>0){
                        v=v.trim();
                        while(v.startsWith("$")){v=v.substring(1)};
                        while(v.endsWith(":")){v=v.substring(0,v.length-1)};
                        v="$"+v;
                        this.setValue(v);
                    }
                }
            },
            {name:"value",
                blur:function(){
                    var v=value=this.getValue();
                    if(v&&v.length>0){
                        v=v.trim();
                        while(v.endsWith(";")){v=v.substring(0,v.length-1)};
                        if(v!=value)this.setValue(v);
                        if(v.startsWith("$")&&!v.contains(" ")){
                            var _form=this.form;
                            skinVariables.fetchData({"name":v},function(response,data){
                                if(data&&data.length==1){
                                    _form.getItem("derivesFrom").setValue(data[0].name);
                                }
                            });
                        }
                    }
                }
            },
            {name:"derivesFrom",
                editorProperties:{autoFetchData:true,addUnknownValues:false},
                editorType:"ComboBoxItem",
                optionDataSource:skinVariables,
                valueField:"name",
                displayField:"name"
            },
            {name:"outputGroup",
                editorProperties:{
                    dataSetType:"tree",
                    valueField:"name",
                    displayField:"title"
                }
            },
            {name:"outputIndex"},
            {name:"category"}
        ];
        this.addAutoChild("editor",{dataSource:this.dataSource,fields:this.defaultFields});
        this.addMember(this.editor);
        if(this.record)this.editRecord(this.record);
        this.disableUI(true);
    }
,isc.A.disableUI=function isc_SkinVariableEditor_disableUI(disable){
        this.addButton.setDisabled(!disable);
        this.editButton.setDisabled(!disable);
        this.renameButton.setDisabled(!disable);
        this.saveButton.setDisabled(disable);
        this.cancelButton.setDisabled(disable);
        this.editor.setDisabled(disable);
    }
,isc.A.setDataSource=function isc_SkinVariableEditor_setDataSource(dataSource){
        this.dataSource=dataSource;
        this.editor.setDataSource(this.dataSource,isc.shallowClone(this.defaultFields));
    }
,isc.A.editRecord=function isc_SkinVariableEditor_editRecord(record){
        this._mode="edit";
        this.editor.editRecord(record);
    }
,isc.A.cancelEdit=function isc_SkinVariableEditor_cancelEdit(){
        this._mode="none";
        this.disableUI(true);
        this.editor.resetValues();
    }
,isc.A.getNextDSId=function isc_SkinVariableEditor_getNextDSId(){
        var lastId=this.creator.grid.data.allRows.getProperty("id").max();
        return lastId+1;
    }
,isc.A.addRecord=function isc_SkinVariableEditor_addRecord(defaults){
        defaults=defaults||{};
        if(defaults.id==null){
            defaults.id=this.getNextDSId();
        }
        defaults.exclude=false;
        this._mode="add";
        this.disableUI(false);
        this.editor.editNewRecord(defaults);
    }
,isc.A.clearRecord=function isc_SkinVariableEditor_clearRecord(){
        this._mode=null;
        this.disableUI(true);
        this.editor.clearValues();
    }
,isc.A.renameVariable=function isc_SkinVariableEditor_renameVariable(){
        var series=isc.SkinFunc.currentSeries,
            attr=this.editor.getItem("name").getValue()
        ;
        isc.logWarn("name is "+attr);
        isc.SkinFunc.editVariableName(attr,series,this.creator.variablesGrid);
    }
,isc.A.saveRecord=function isc_SkinVariableEditor_saveRecord(defaults){
        var _this=this;
        this.editor.saveData(function(dsResponse,data){
            if(dsResponse.status==0){
                isc.say("Variable saved.");
                _this.clearRecord();
                if(_this._mode=="add")_this.recordAdded(data);
                else _this.recordSaved(data);
            }
        });
    }
,isc.A.recordAdded=function isc_SkinVariableEditor_recordAdded(record){
    }
,isc.A.recordSaved=function isc_SkinVariableEditor_recordSaved(record){
    }
);
isc.B._maxIndex=isc.C+12;

isc.defineClass("SkinColorEditor","DynamicForm");
isc.A=isc.SkinColorEditor.getPrototype();
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.membersMargin=10;
isc.A.layoutMargin=10;
isc.A.buttonLayoutDefaults={
        _constructor:"HLayout",
        width:"100%",
        height:1,
        membersMargin:10
    };
isc.A.addButtonDefaults={
        _constructor:"IButton",
        icon:"[SKINIMG]actions/add.png",
        title:"Add Variable",
        click:function(){
            this.creator.addRecord();
        }
    };
isc.A.saveButtonDefaults={
        _constructor:"IButton",
        icon:"[SKINIMG]actions/save.png",
        title:"Save",
        click:function(){
            this.creator.saveRecord();
        }
    };
isc.A.editorDefaults={
        _constructor:"DynamicForm",
        width:"100%",
        numCols:6
    };
isc.B.push(isc.A.initWidget=function isc_SkinColorEditor_initWidget(){
        this.Super("initWidget",arguments);
        this.addAutoChild("buttonLayout");
        this.addMember(this.buttonLayout);
        this.addAutoChildren(["addButton","saveButton"]);
        this.buttonLayout.addMembers([this.addButton,this.saveButton]);
        this.addAutoChild("editor",{dataSource:this.dataSource});
        this.addMember(this.editor);
    }
,isc.A.setDataSource=function isc_SkinColorEditor_setDataSource(dataSource){
        this.dataSource=dataSource;
        this.editor.setDataSource(this.dataSource);
    }
,isc.A.editRecord=function isc_SkinColorEditor_editRecord(record){
        this._mode="edit";
        this.editor.editRecord(record);
    }
,isc.A.addRecord=function isc_SkinColorEditor_addRecord(defaults){
        this._mode="add";
        this.editor.editNewRecord(defaults);
    }
,isc.A.clearRecord=function isc_SkinColorEditor_clearRecord(){
        this._mode=null;
        this.editor.clearValues();
    }
,isc.A.saveRecord=function isc_SkinColorEditor_saveRecord(defaults){
        var _this=this;
        this.editor.saveData(function(dsResponse,data){
            if(!dsResponse.status==0){
                isc.say("Variable saved.");
                _this.clearRecord();
                if(_this._mode=="add")_this.recordAdded(data);
                else _this.recordSaved(data);
            }
        });
    }
,isc.A.recordAdded=function isc_SkinColorEditor_recordAdded(record){
    }
,isc.A.recordSaved=function isc_SkinColorEditor_recordSaved(record){
    }
);
isc.B._maxIndex=isc.C+8;

isc.defineClass("ColorTransformer","VLayout");
isc.A=isc.ColorTransformer.getPrototype();
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.autoDraw=true;
isc.A.formDefaults={
        _constructor:"DynamicForm",
        numCols:6,
        minWidth:600,
        minColWidth:900,
        colWidths:[100,120,100,120,100,120],
        fields:[
            {name:"inputColor",width:"*",editorType:"ColorItem",title:"Color",
                changed:function(form,item,value){
                    form.creator.setColor(value);
                }
            },
            {name:"transform",editorType:"SelectItem",title:"Transform",width:"*",
                valueMap:isc.SkinFunc.transformTypes,
                changed:function(form,item,value){
                    if(value=="custom"||value=="none"){
                        form.hideItem("transformDelta");
                        if(value=="custom"){
                            form.showItem("customTransform");
                        }
                    }else{
                        form.hideItem("customTransform");
                        form.showItem("transformDelta");
                    }
                    form.creator.changeTransform(value);
                }
            },
            {name:"transformedColor",editorType:"ColorItem",width:"*",title:"Result"},
            {name:"transformDelta",type:"integer",width:"*",editorType:"SliderItem",
                title:"Change By %",
                height:30,
                min:0,max:100,
                colSpan:"*",
                sliderProperties:{showRange:false},
                visible:false,
                changed:function(form,item,value){
                    form.creator.changeTransformDelta(value);
                }
            },
            {
                name:"customTransform",
                editorType:"TextAreaItem",title:"Custom",
                showHintInField:true,
                hint:"Javascript function-body that returns a color value",
                width:"*",
                height:60,
                colSpan:"*",
                visible:false,
                changeOnKeypress:false,
                changeOnEnter:true,
                changed:function(form,item,value){
                    form.creator.changeCustomTransform(value);
                }
            }
        ]
    };
isc.A.transform="none";
isc.A.transformDelta=0;
isc.B.push(isc.A.initWidget=function isc_ColorTransformer_initWidget(){
        this.Super("initWidget",arguments);
        this.addAutoChild("form");
        this.addMember(this.form);
        if(this.color)this.setColor(this.color);
    }
,isc.A.setColor=function isc_ColorTransformer_setColor(color){
        this.inputColor=color;
        this.color=isc.SkinUtil.getSkinColor(color);
        if(this.transformDelta)this.startTransformDelta=this.transformDelta;
        this.updateForm();
    }
,isc.A.changeTransform=function isc_ColorTransformer_changeTransform(transform){
        this.transform=transform;
        this.updateForm();
    }
,isc.A.changeTransformDelta=function isc_ColorTransformer_changeTransformDelta(delta){
        this.transformDelta=delta;
        this.updateForm();
    }
,isc.A.changeCustomTransform=function isc_ColorTransformer_changeCustomTransform(value){
        this.customTransform=value;
        this.updateForm();
    }
,isc.A.updateForm=function isc_ColorTransformer_updateForm(){
        var c=this.color;
        if(c.valid){
            var hexColor=c.toHexString();
            if(this.transform=="custom"){
                var theFunc=isc.isA.Function(this.customTransform)?this.customTransform:
                        isc._makeFunction("color",this.customTransform);
                this.transformedColor=theFunc.apply(this,[isc.SkinUtil.getSkinColor(hexColor)]);
            }else if(this.transform=="none"){
                this.transformedColor=hexColor;
            }else{
                this.transformedColor=
                    isc.SkinFunc.transformColor(hexColor,this.transform,this.transformDelta);
            }
            this.form.setValues({
                "inputColor":hexColor,
                "transform":this.transform,
                "transformDelta":this.transformDelta,
                "customTransform":this.customTransform,
                "transformedColor":this.transformedColor
            });
        }else{
            this.form.setValues({"inputColor":this.inputColor});
        }
        if(this.transform=="custom")this.form.showItem("customTransform")
        else if(this.transform!="none"){
            this.form.showItem("transformDelta");
        }
    }
);
isc.B._maxIndex=isc.C+6;

isc.defineClass("ColorTransformerItem","CanvasItem");
isc.A=isc.ColorTransformerItem.getPrototype();
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.height=1;
isc.A.overflow="visible";
isc.A.canvasConstructor="ColorTransformer";
isc.B.push(isc.A.setValue=function isc_ColorTransformerItem_setValue(newValue){
        var arr=newValue==null?null:newValue.split("::");
        if(arr&&arr.length>0){
            this.canvas.transformDelta=0;
            this.canvas.customTransform=null;
            this.canvas.transform=arr[1];
            if(arr[1]!="none"){
                if(arr.length>2){
                    if(this.transform=="custom"){
                        this.canvas.customTransform=arr[1];
                    }else{
                        this.canvas.transformDelta=arr[1];
                    }
                }
            }
            this.canvas.setColor(arr[0]);
        }
    }
,isc.A.getValue=function isc_ColorTransformerItem_getValue(){
        var f=this.canvas,
            result=f.transform;
        if(result!="none"){
            result+="::";
            if(result=="custom"){
                result+=f.customTransform;
            }else{
                result+=f.transformDelta;
            }
        }
        return result;
    }
);
isc.B._maxIndex=isc.C+2;

isc.defineClass("SkinVariableGroupEditor","VLayout");
isc.A=isc.SkinVariableGroupEditor.getPrototype();
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.buttonLayoutDefaults={
        _constructor:"HLayout",
        width:"100%",
        height:1
    };
isc.A.addButtonDefaults={
        _constructor:"IButton",
        title:"Add Group",
        autoFit:true,
        autoParent:"buttonLayout",
        click:function(){
            this.creator.addGroup();
        }
    };
isc.A.editButtonDefaults={
        _constructor:"IButton",
        title:"Edit",
        autoFit:true,
        autoParent:"buttonLayout",
        click:function(){
            this.creator.startEditing();
        }
    };
isc.A.saveButtonDefaults={
        _constructor:"IButton",
        title:"Save",
        autoFit:true,
        autoParent:"buttonLayout",
        click:function(){
            this.creator.saveRecord();
        }
    };
isc.A.cancelButtonDefaults={
        _constructor:"IButton",
        title:"Cancel",
        autoFit:true,
        autoParent:"buttonLayout",
        click:function(){
            this.creator.cancelEdit();
        }
    };
isc.A.formDefaults={
        _constructor:"DynamicForm",
        fields:[
            {name:"parentId",title:"Group",valueField:"name",displayField:"title"},
            {name:"name"},
            {name:"title"},
            {name:"comment"},
            {name:"index"}
        ]
    };
isc.B.push(isc.A.initWidget=function isc_SkinVariableGroupEditor_initWidget(){
        this.Super("initWidget",arguments);
        this.addAutoChild("buttonLayout");
        this.addMember(this.buttonLayout);
        this.addAutoChildren(["addButton","editButton","saveButton","cancelButton"]);
        this.addAutoChild("form",{dataSource:skinVariableGroups});
        this.addMember(this.form);
    }
,isc.A.editRecord=function isc_SkinVariableGroupEditor_editRecord(record){
        this.form.editRecord(record);
    }
,isc.A.editNewRecord=function isc_SkinVariableGroupEditor_editNewRecord(record){
        this.form.editNewRecord(record);
    }
,isc.A.saveRecord=function isc_SkinVariableGroupEditor_saveRecord(){
        this.form.saveData();
        this.hide();
    }
,isc.A.cancelEdit=function isc_SkinVariableGroupEditor_cancelEdit(){
        this.hide();
    }
);
isc.B._maxIndex=isc.C+5;

isc.defineClass("SkinVariableGroupTree","TreeGrid");
isc.A=isc.SkinVariableGroupTree.getPrototype();
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.dataSource="skinVariableGroups";
isc.A.autoFetchData=true;
isc.A.canAcceptDroppedRecords=true;
isc.A.canReparentNodes=true;
isc.A.dataProperties={
        fetchMode:"local",
        loadDataOnDemand:false,
        autoOpenRoot:"root"
    };
isc.A.fields=[
        {name:"name",showIf:"false"},
        {name:"title",width:"*"},
        {name:"index",width:70}
    ];
isc.A.initialSort=[{property:"index",direction:"ascending"}];
isc.A.sortByGroupfirst=true;
isc.A.groupEditorDefaults={
        _constructor:"SkinVariableGroupEditor",
        backgroundColor:"white"
    };
isc.A.contextMenu={
        _constructor:"Menu",
        autoDraw:false,
        data:[
            {title:"Add Child Group",
                click:function(target,item,menu,colNum){
                    target.addChildGroup(target.getSelectedRecord());
                }
            },
            {title:"Add Root Group",
                click:function(target,item,menu,colNum){
                    target.addChildGroup(null);
                }
            },
            {title:"Edit Group",
                click:function(target,item,menu,colNum){
                    target.editGroup(target.getSelectedRecord());
                }
            }
        ],
        width:150
    };
isc.B.push(isc.A.dataArrived=function isc_SkinVariableGroupTree_dataArrived(){
        this.data.openAll();
        var list=this.data.getOpenList();
        list.map(function(item){item.isFolder=true});
    }
,isc.A.dataChanged=function isc_SkinVariableGroupTree_dataChanged(){
        this.data.openAll();
        var list=this.data.getOpenList();
        list.map(function(item){item.isFolder=true});
    }
,isc.A.nodeClick=function isc_SkinVariableGroupTree_nodeClick(viewer,node,recordNum){
        if(!this.data.isOpen(node))this.openFolder(node);
        this.Super("nodeClick",arguments);
    }
,isc.A.initWidget=function isc_SkinVariableGroupTree_initWidget(){
        this.Super("initWidget",arguments);
        this.nodeIcon=this.folderIcon;
    }
,isc.A.getGroupEditor=function isc_SkinVariableGroupTree_getGroupEditor(){
        if(!this.groupEditor){
            this.groupEditor=this.createAutoChild("groupEditor");
        }
        return this.groupEditor;
    }
,isc.A.addChildGroup=function isc_SkinVariableGroupTree_addChildGroup(parent){
        var ed=this.getGroupEditor(),
            props={}
        ;
        if(parent){
            props.parentId=parent.name;
            props.name=parent.name+"_";
        }
        ed.editNewRecord(props);
        this.showEditor();
    }
,isc.A.showEditor=function isc_SkinVariableGroupTree_showEditor(){
        var ed=this.getGroupEditor();
        if(!ed.isDrawn())ed.draw();
        ed.moveTo((isc.Page.getWidth()/2)-(ed.getWidth()/2),(isc.Page.getHeight()/2)-(ed.getHeight()/2));
        ed.show();
    }
,isc.A.editGroup=function isc_SkinVariableGroupTree_editGroup(record){
        var ed=this.getGroupEditor();
        ed.editRecord(record);
        this.showEditor();
    }
);
isc.B._maxIndex=isc.C+8;

isc.defineClass("SkinVariableGroupLayout","VLayout");
isc.A=isc.SkinVariableGroupLayout.getPrototype();
isc.B=isc._allFuncs;
isc.C=isc.B._maxIndex;
isc.D=isc._funcClasses;
isc.D[isc.C]=isc.A.Class;
isc.A.groupTreeDefaults={
        _constructor:"SkinVariableGroupTree",
        width:300,
        showResizeBar:true,
        autoParent:"bodyLayout",
        canEdit:true,
        nodeClick:function(viewer,node,recordNum){
            this.creator.showGroupVariables(node);
        }
    };
isc.A.gridDefaults={
        _constructor:"ListGrid",
        width:"*",
        dataSource:"skinVariables",
        autoFetchData:true,
        dataFetchMode:"local",
        canEdit:true,
        autoParent:"bodyLayout",
        initialSort:{property:"outputIndex",direction:"ascending"},
        canDragRecordsOut:true,
        fields:[
            {name:"name",width:300},
            {name:"value",width:300},
            {name:"comment",width:300},
            {name:"outputIndex"}
        ]
    };
isc.A.bodyLayoutDefaults={
        _constructor:"HLayout",
        width:"100%",
        height:"*"
    };
isc.B.push(isc.A.initWidget=function isc_SkinVariableGroupLayout_initWidget(){
        this.Super("initWidget",arguments);
        this.addAutoChild("bodyLayout");
        this.addMember(this.bodyLayout);
        this.addAutoChildren(["groupTree","grid"]);
    }
,isc.A.showGroupVariables=function isc_SkinVariableGroupLayout_showGroupVariables(folder){
        this.grid.fetchData({fieldName:"outputGroup",operator:"iStartsWith",value:folder.name},null,{invalidateCache:true});
    }
,isc.A.getGroupEditor=function isc_SkinVariableGroupLayout_getGroupEditor(){
        return this.groupTree&&this.groupTree.groupEditor;
    }
,isc.A.addChildGroup=function isc_SkinVariableGroupLayout_addChildGroup(parent){
        if(this.groupTree)this.groupTree.addChildGroup(parent);
    }
,isc.A.editGroupRecord=function isc_SkinVariableGroupLayout_editGroupRecord(record){
        if(this.groupTree)this.groupTree.editGroup(record);
    }
);
isc.B._maxIndex=isc.C+5;

isc.getButtonsPane=function(){
        isc.Menu.create({
        ID:"menu",
        autoDraw:false,
        showShadow:true,
        shadowDepth:10,
        data:[
            {title:"New",keyTitle:"Ctrl+N",icon:"icons/16/document_plain_new.png"},
            {title:"Open",keyTitle:"Ctrl+O",icon:"icons/16/folder_out.png"},
            {isSeparator:true},
            {title:"Save",keyTitle:"Ctrl+S",icon:"icons/16/disk_blue.png"},
            {title:"Save As",icon:"icons/16/save_as.png"},
            {isSeparator:true},
            {title:"Recent Documents",icon:"icons/16/folder_document.png",submenu:[
                {title:"data.xml",checked:true},
                {title:"Component Guide.doc"},
                {title:"SmartClient.doc",checked:true},
                {title:"AJAX.doc"}
            ]},
            {isSeparator:true},
            {title:"Export as...",icon:"icons/16/export1.png",submenu:[
                {title:"XML"},
                {title:"CSV"},
                {title:"Plain text"}
            ]},
            {isSeparator:true},
            {title:"Print",enabled:false,keyTitle:"Ctrl+P",icon:"icons/16/printer3.png"}
        ]
    });
    isc.ToolStripMenuButton.create({
        ID:"menuButton",
        title:"File",
        menu:menu
    });
    isc.ToolStripButton.create({
        ID:"printButton",
        icon:"[SKIN]/RichTextEditor/print.png",
        title:"Print"
    });
    isc.ToolStripButton.create({
        ID:"alignLeft",
        icon:"[SKIN]/RichTextEditor/text_align_left.png",
        actionType:"radio",
        radioGroup:"textAlign"
    });
    isc.ToolStripButton.create({
        ID:"alignRight",
        icon:"[SKIN]/RichTextEditor/text_align_right.png",
        actionType:"radio",
        radioGroup:"textAlign"
    });
    isc.ToolStripButton.create({
        ID:"alignCenter",
        icon:"[SKIN]/RichTextEditor/text_align_center.png",
        actionType:"radio",
        radioGroup:"textAlign"
    });
    isc.ToolStripButton.create({
        ID:"bold",
        icon:"[SKIN]/RichTextEditor/text_bold.png",
        actionType:"checkbox",
        showFocused:false,
        showFocusOutline:true
    });
    isc.ToolStripButton.create({
        ID:"italics",
        icon:"[SKIN]/RichTextEditor/text_italic.png",
        actionType:"checkbox",
        showFocused:false,
        showFocusOutline:true
    });
    isc.ToolStripButton.create({
        ID:"underlined",
        icon:"[SKIN]/RichTextEditor/text_underline.png",
        actionType:"checkbox",
        showFocused:false,
        showFocusOutline:true
    });
    isc.DynamicForm.create({
        ID:"fontSelector",
        showResizeBar:true,
        width:120,minWidth:50,
        numCols:1,
        fields:[
            {name:"selectFont",showTitle:false,width:"*",
             valueMap:{
                "courier":"<span style='font-family:courier'>Courier</span>",
                "verdana":"<span style='font-family:verdana'>Verdana</span>",
                "times":"<span style='font-family:times'>Times</span>"
             },defaultValue:"courier"}
        ]
    });
    isc.DynamicForm.create({
        ID:"zoomSelector",
        width:100,minWidth:50,
        numCols:1,
        fields:[
            {name:"selectZoom",showTitle:false,width:"*",
             valueMap:["50%","75%","100%","150%","200%","Fit"],
             defaultValue:"100%"}
        ]
    });
    isc.ToolStrip.create({
        ID:"toolStrip",
        members:[menuButton,"separator",printButton,
                  "resizer",bold,italics,underlined,
                  "separator",
                  alignLeft,alignRight,alignCenter,
                  "separator",
                  fontSelector,"resizer",zoomSelector]
    });
    var typeMenu={
        _constructor:"Menu",
        autoDraw:false,
        showShadow:true,
        shadowDepth:10,
        data:[
            {title:"Document",keyTitle:"Ctrl+D",icon:"[SKINIMG]actions/edit.png"},
            {title:"Picture",keyTitle:"Ctrl+P",icon:"[SKINIMG]actions/close.png"},
            {title:"Email",keyTitle:"Ctrl+E",icon:"[SKINIMG]actions/accept.png"}
        ]
    };
    function getIconButton(title,props){
        return isc.IconButton.create(isc.addProperties({
                title:title,
                icon:"[SKINIMG]actions/add.png",
                largeIcon:"[SKINIMG]actions/add.png",
                click:"isc.say(this.title + ' button clicked');"
            },props)
        );
    }
    function getIconMenuButton(title,props){
        return isc.IconMenuButton.create(isc.addProperties({
                title:title,
                icon:"[SKINIMG]actions/edit.png",
                largeIcon:"[SKINIMG]actions/edit.png",
                click:"isc.say(this.title + ' button clicked');"
            },props)
        );
    }
    isc.RibbonGroup.create({
        ID:"fileGroup",
        title:"File (vertical icons)",
        numRows:3,
        colWidths:[40,"*"],
        titleAlign:"left",
        controls:[
            getIconMenuButton("New",{orientation:"vertical",menu:typeMenu,showMenuIconOver:false}),
            getIconButton("Open",{orientation:"vertical",largeIcon:"[SKINIMG]actions/accept.png"}),
            getIconButton("Save",{orientation:"vertical",largeIcon:"[SKINIMG]actions/save.png"}),
            getIconMenuButton("Save As",{orientation:"vertical",menu:typeMenu,largeIcon:"[SKINIMG]actions/add.png"})
        ],
        autoDraw:false
    });
    isc.RibbonGroup.create({
        ID:"editGroup",
        title:"Editing Tools",
        numRows:3,
        colWidths:[40,"*"],
        controls:[
            getIconButton("Edit",{icon:"[SKINIMG]actions/edit.png"}),
            getIconButton("Copy",{icon:"[SKINIMG]actions/refresh.png"}),
            getIconButton("Paste"),
            getIconMenuButton("Undo",{menu:typeMenu,showMenuIconOver:false,icon:"[SKINIMG]actions/add.png"}),
            getIconMenuButton("Redo",{menu:typeMenu,icon:"[SKINIMG]actions/accept.png"})
        ],
        autoDraw:false
    });
    isc.RibbonGroup.create({
        ID:"insertGroup",
        title:"Insert",
        numRows:3,
        colWidths:[40,"*"],
        controls:[
            getIconMenuButton("Picture",{orientation:"vertical",menu:typeMenu,largeIcon:"[SKINIMG]actions/add.png"}),
            getIconButton("Link",{icon:"[SKINIMG]actions/edit.png"}),
            getIconButton("Document",{icon:"[SKINIMG]actions/accept.png"}),
            getIconButton("Video",{icon:"[SKINIMG]actions/help.png"})
        ],
        autoDraw:false
    });
    isc.RibbonBar.create({
        ID:"ribbonBar",
        groupTitleAlign:"center",
        groupTitleOrientation:"top"
    });
    ribbonBar.addGroup(fileGroup,0);
    ribbonBar.addGroup(editGroup,1);
    ribbonBar.addGroup(insertGroup,2);
    isc.Menu.create({
        ID:"customColumnMenu",
        autoDismiss:false,
        fields:[
            "title",
            {name:"canDismiss",width:16,
             showValueIconOnly:true,
             valueIcons:{
                "true":"[SKINIMG]actions/close.png"
             }
            }
        ],
        data:[
            {name:"Item 1"},
            {name:"Item 2",canDismiss:true},
            {name:"Item 3",canDismiss:true}
        ],
        itemClick:function(item,colNum){
            if(colNum==1&&item.canDismiss){
                this.removeItem(item);
            }else{
                isc.say("You Selected '"+item.name+"'.");
                this.hide();
            }
        }
    });
    isc.MenuButton.create({
        ID:"menuButton1",
        autoDraw:false,
        title:"Show Menu",
        menu:customColumnMenu
    });
    isc.Button.create({
        ID:"buttonCSSButton",
        autoFit:true,
        title:"CSS Button",
        icon:"[SKINIMG]actions/add.png"
    });
    isc.IButton.create({
        ID:"buttonIButton",
        title:"Stretch Button",
        width:150,
        icon:"[SKINIMG]actions/find.png"
    });
    isc.ImgButton.create({
        ID:"buttonImgButton",
        width:18,
        height:18,
        src:"[SKIN]/ImgButton/button.png"
    });
    isc.NavigationBar.create({
        ID:"navBar1",
        width:500,
        leftButtonTitle:"Back",
        rightButtonTitle:"Forward",
        showRightButton:true,
        title:"NavBar Title",
        navigationClick:function(direction){
            if(direction==="back"){
                isc.say("Back button clicked!");
            }else if(direction==="forward"){
                isc.say("Forward button clicked!");
            }
        }
    });
    isc.HLayout.create({
        ID:"buttonRowLayout1",
        membersMargin:20,
        members:[ribbonBar]
    });
    isc.HLayout.create({
        ID:"buttonRowLayout2",
        membersMargin:20,
        members:[menuButton1,buttonCSSButton,buttonIButton,buttonImgButton]
    });
    isc.VLayout.create({
        ID:"buttonsPane",
        layoutMargin:10,
        membersMargin:20,
        top:60,
        autoDraw:false,
        members:[navBar1,toolStrip,buttonRowLayout1,buttonRowLayout2]
    });
    return buttonsPane;
}
isc.getCubesPane=function(){
    var productData=[
        {quarter:"Q1, 2016",month:"January",region:"Western U.S.",product:"Pens",metric:"Revenue",_value:10000,percentNational:25},
        {quarter:"Q1, 2016",month:"January",region:"Western U.S.",product:"Chairs",metric:"Revenue",_value:50000,percentNational:45},
        {quarter:"Q1, 2016",month:"January",region:"Western U.S.",product:"Monitors",metric:"Revenue",_value:120000,percentNational:49},
        {quarter:"Q1, 2016",month:"January",region:"Western U.S.",product:"Pens",metric:"Profit",_value:2000,percentNational:25},
        {quarter:"Q1, 2016",month:"January",region:"Western U.S.",product:"Chairs",metric:"Profit",_value:5000,percentNational:45},
        {quarter:"Q1, 2016",month:"January",region:"Western U.S.",product:"Monitors",metric:"Profit",_value:44000,percentNational:59,_hilite:"over50"},
        {quarter:"Q1, 2016",month:"January",region:"Midwest U.S.",product:"Pens",metric:"Revenue",_value:8000,percentNational:20},
        {quarter:"Q1, 2016",month:"January",region:"Midwest U.S.",product:"Chairs",metric:"Revenue",_value:22000,percentNational:20},
        {quarter:"Q1, 2016",month:"January",region:"Midwest U.S.",product:"Monitors",metric:"Revenue",_value:20000,percentNational:8,_hilite:"under10"},
        {quarter:"Q1, 2016",month:"January",region:"Midwest U.S.",product:"Pens",metric:"Profit",_value:2000,percentNational:25},
        {quarter:"Q1, 2016",month:"January",region:"Midwest U.S.",product:"Chairs",metric:"Profit",_value:2000,percentNational:18},
        {quarter:"Q1, 2016",month:"January",region:"Midwest U.S.",product:"Monitors",metric:"Profit",_value:5000,percentNational:7,_hilite:"under10"},
        {quarter:"Q1, 2016",month:"January",region:"Eastern U.S.",product:"Pens",metric:"Revenue",_value:22000,percentNational:55,_hilite:"over50"},
        {quarter:"Q1, 2016",month:"January",region:"Eastern U.S.",product:"Chairs",metric:"Revenue",_value:40000,percentNational:36},
        {quarter:"Q1, 2016",month:"January",region:"Eastern U.S.",product:"Monitors",metric:"Revenue",_value:105000,percentNational:43},
        {quarter:"Q1, 2016",month:"January",region:"Eastern U.S.",product:"Pens",metric:"Profit",_value:4000,percentNational:50,_hilite:"over50"},
        {quarter:"Q1, 2016",month:"January",region:"Eastern U.S.",product:"Chairs",metric:"Profit",_value:4000,percentNational:36},
        {quarter:"Q1, 2016",month:"January",region:"Eastern U.S.",product:"Monitors",metric:"Profit",_value:25000,percentNational:34},
        {quarter:"Q1, 2016",month:"February",region:"Western U.S.",product:"Pens",metric:"Revenue",_value:12000,percentNational:23},
        {quarter:"Q1, 2016",month:"February",region:"Western U.S.",product:"Chairs",metric:"Revenue",_value:42000,percentNational:47},
        {quarter:"Q1, 2016",month:"February",region:"Western U.S.",product:"Monitors",metric:"Revenue",_value:160000,percentNational:40},
        {quarter:"Q1, 2016",month:"February",region:"Western U.S.",product:"Pens",metric:"Profit",_value:4000,percentNational:23},
        {quarter:"Q1, 2016",month:"February",region:"Western U.S.",product:"Chairs",metric:"Profit",_value:4000,percentNational:47},
        {quarter:"Q1, 2016",month:"February",region:"Western U.S.",product:"Monitors",metric:"Profit",_value:68000,percentNational:40},
        {quarter:"Q1, 2016",month:"February",region:"Midwest U.S.",product:"Pens",metric:"Revenue",_value:10000,percentNational:19},
        {quarter:"Q1, 2016",month:"February",region:"Midwest U.S.",product:"Chairs",metric:"Revenue",_value:12000,percentNational:13},
        {quarter:"Q1, 2016",month:"February",region:"Midwest U.S.",product:"Monitors",metric:"Revenue",_value:75000,percentNational:19},
        {quarter:"Q1, 2016",month:"February",region:"Midwest U.S.",product:"Pens",metric:"Profit",_value:3000,percentNational:20},
        {quarter:"Q1, 2016",month:"February",region:"Midwest U.S.",product:"Chairs",metric:"Profit",_value:1000,percentNational:11},
        {quarter:"Q1, 2016",month:"February",region:"Midwest U.S.",product:"Monitors",metric:"Profit",_value:32000,percentNational:17},
        {quarter:"Q1, 2016",month:"February",region:"Eastern U.S.",product:"Pens",metric:"Revenue",_value:31000,percentNational:58,_hilite:"over50"},
        {quarter:"Q1, 2016",month:"February",region:"Eastern U.S.",product:"Chairs",metric:"Revenue",_value:35000,percentNational:39},
        {quarter:"Q1, 2016",month:"February",region:"Eastern U.S.",product:"Monitors",metric:"Revenue",_value:164000,percentNational:41},
        {quarter:"Q1, 2016",month:"February",region:"Eastern U.S.",product:"Pens",metric:"Profit",_value:8000,percentNational:53,_hilite:"over50"},
        {quarter:"Q1, 2016",month:"February",region:"Eastern U.S.",product:"Chairs",metric:"Profit",_value:4000,percentNational:44},
        {quarter:"Q1, 2016",month:"February",region:"Eastern U.S.",product:"Monitors",metric:"Profit",_value:88000,percentNational:47},
        {quarter:"Q1, 2016",month:"March",region:"Western U.S.",product:"Pens",metric:"Revenue",_value:18000,percentNational:26},
        {quarter:"Q1, 2016",month:"March",region:"Western U.S.",product:"Chairs",metric:"Revenue",_value:25000,percentNational:54,_hilite:"over50"},
        {quarter:"Q1, 2016",month:"March",region:"Western U.S.",product:"Monitors",metric:"Revenue",_value:220000,percentNational:40},
        {quarter:"Q1, 2016",month:"March",region:"Western U.S.",product:"Pens",metric:"Profit",_value:9000,percentNational:29},
        {quarter:"Q1, 2016",month:"March",region:"Western U.S.",product:"Chairs",metric:"Profit",_value:2000,percentNational:40},
        {quarter:"Q1, 2016",month:"March",region:"Western U.S.",product:"Monitors",metric:"Profit",_value:112000,percentNational:38},
        {quarter:"Q1, 2016",month:"March",region:"Midwest U.S.",product:"Pens",metric:"Revenue",_value:7000,percentNational:10},
        {quarter:"Q1, 2016",month:"March",region:"Midwest U.S.",product:"Chairs",metric:"Revenue",_value:6000,percentNational:13},
        {quarter:"Q1, 2016",month:"March",region:"Midwest U.S.",product:"Monitors",metric:"Revenue",_value:135000,percentNational:25},
        {quarter:"Q1, 2016",month:"March",region:"Midwest U.S.",product:"Pens",metric:"Profit",_value:2000,percentNational:6,_hilite:"under10"},
        {quarter:"Q1, 2016",month:"March",region:"Midwest U.S.",product:"Chairs",metric:"Profit",_value:1000,percentNational:20},
        {quarter:"Q1, 2016",month:"March",region:"Midwest U.S.",product:"Monitors",metric:"Profit",_value:66000,percentNational:23},
        {quarter:"Q1, 2016",month:"March",region:"Eastern U.S.",product:"Pens",metric:"Revenue",_value:44000,percentNational:64,_hilite:"over50"},
        {quarter:"Q1, 2016",month:"March",region:"Eastern U.S.",product:"Chairs",metric:"Revenue",_value:15000,percentNational:33},
        {quarter:"Q1, 2016",month:"March",region:"Eastern U.S.",product:"Monitors",metric:"Revenue",_value:190000,percentNational:35},
        {quarter:"Q1, 2016",month:"March",region:"Eastern U.S.",product:"Pens",metric:"Profit",_value:20000,percentNational:65,_hilite:"over50"},
        {quarter:"Q1, 2016",month:"March",region:"Eastern U.S.",product:"Chairs",metric:"Profit",_value:2000,percentNational:40},
        {quarter:"Q1, 2016",month:"March",region:"Eastern U.S.",product:"Monitors",metric:"Profit",_value:115000,percentNational:39}
    ];
    isc.CubeGrid.create({
        ID:"cubeBasic",
        data:productData,
        width:800,
        height:400,
        hideEmptyFacetValues:true,
        valueFormat:"\u00A4,0.00",
        columnFacets:["quarter","month","metric"],
        rowFacets:["region","product"]
    });
    isc.HLayout.create({
        ID:"cubeRowLayout1",
        membersMargin:20,
        members:[cubeBasic]
    });
    isc.VLayout.create({
        ID:"cubesPane",
        layoutMargin:10,
        membersMargin:20,
        top:60,
        autoDraw:false,
        members:[cubeRowLayout1]
    });
    return cubesPane;
}
isc.getFormItemsPane=function(){
function fUpdateReadOnlyDisplay(mode){
    mode=mode||readOnlyDisplayPickerForm.getValue("readOnlyDisplayPicker");
    favoritesForm.setReadOnlyDisplay(mode);
    form1.setReadOnlyDisplay(mode);
    form2.setReadOnlyDisplay(mode);
    dateForm.setReadOnlyDisplay(mode);
}
function fSetCanEdit(canEdit){
    fUpdateReadOnlyDisplay();
    favoritesForm.setCanEdit(canEdit);
    form1.setCanEdit(canEdit);
    form2.setCanEdit(canEdit);
    dateForm.setCanEdit(canEdit);
}
isc.IButton.create({
    ID:"toggleCanEdit",
    title:"setCanEdit(false)",
    autoFit:true,
    click:function(){
        if(this.title=="setCanEdit(false)")this.setTitle("setCanEdit(true)");
        else this.setTitle("setCanEdit(false)");
        var canEdit=this.title=="setCanEdit(false)";
        fSetCanEdit(canEdit);
    }
});
isc.DynamicForm.create({
    ID:"readOnlyDisplayPickerForm",
    numCols:2,
    colWidths:[120,140],
    wrapItemTitles:false,
    fields:[
        {name:"readOnlyDisplayPicker",title:"ReadOnly Display",width:"*",
            valueMap:["static","readOnly","disabled"],
            defaultValue:"disabled",
            changed:function(){
                fUpdateReadOnlyDisplay(this.getValue());
            }
        }
    ]
});
isc.ToolStrip.create({
    ID:"uiToolstripFI",
    members:[toggleCanEdit,readOnlyDisplayPickerForm]
});
    isc.DynamicForm.create({
        ID:"form1",
        width:360,
        colWidths:[190,"*"],
        fields:[
            {name:"text",title:"Text",type:"text"},
            {name:"colorPicker",title:"Color Picker",type:"color"},
            {name:"textArea",title:"TextArea",type:"textArea"},
            {name:"stackedSpinner",title:"Stacked Spinner",editorType:"SpinnerItem",writeStackedIcons:true,
             defaultValue:5,min:0,max:10,step:0.5,wrapTitle:false},
            {name:"unstackedSpinner",title:"Unstacked Spinner",editorType:"SpinnerItem",writeStackedIcons:false,
             defaultValue:5,min:0,max:10,step:0.5},
            {name:"slider",title:"Slider",editorType:"SliderItem",
             minValue:1,maxValue:5,numValues:5,height:isc.Browser.isTouch?52:36},
            {name:"link",title:"LinkItem",type:"link",height:80,target:"javascript",
             click:"isc.say('Hello world!')",linkTitle:"Click Me"},
            {name:"checkbox",title:"Checkbox",type:"checkbox",height:25},
            {name:"radioGroup",title:"Radio Group",type:"radioGroup",
             valueMap:["Option 1","Option 2"]}
        ],
        values:{slider:4}
    });
    var valueMap={
        "US":"<b>United States</b>",
        "CH":"China",
        "JA":"<b>Japan</b>",
        "IN":"India",
        "GM":"Germany",
        "FR":"France",
        "IT":"Italy",
        "RS":"Russia",
        "BR":"<b>Brazil</b>",
        "CA":"Canada",
        "MX":"Mexico",
        "SP":"Spain"
    }
    var valueIcons={
        "US":"US",
        "CH":"CH",
        "JA":"JA",
        "IN":"IN",
        "GM":"GM",
        "FR":"FR",
        "IT":"IT",
        "RS":"RS",
        "BR":"BR",
        "CA":"CA",
        "MX":"MX",
        "SP":"SP"
    }
    isc.DynamicForm.create({
        ID:"form2",
        width:500,
        colWidths:[170,"*"],
        isGroup:true,
        groupTitle:"Select / Combo Controls",
        fields:[{
            name:"bugStatus",title:"Select",hint:"<nobr>A simple comboBox</nobr>",
            editorType:"ComboBoxItem",
            valueMap:{
                "cat":"Cat",
                "dog":"Dog",
                "giraffe":"Giraffe",
                "goat":"Goat",
                "marmoset":"Marmoset",
                "mouse":"Mouse"
            }
        },
        {
            name:"itemName",title:"Item Name",hint:"<nobr>A databound comboBox</nobr>",
            editorType:"ComboBoxItem",
            optionDataSource:"supplyItem"
        },
        {
            name:"selectItem",title:"Select",hint:"<nobr>A select with icons</nobr>",
            editorType:"SelectItem",
            valueMap:valueMap,
            valueIcons:valueIcons,
            imageURLPrefix:"../../../isomorphic/client/reference/exampleImages/flags/16/",
            imageURLSuffix:".png"
        },
        {
            name:"selectItem2",title:"Select",hint:"<nobr>A select with styled entries</nobr>",
            editorType:"SelectItem",
            valueMap:{
                "red":"<span style='color:#FF0000;'>Red</span>",
                "green":"<span style='color:#00FF00;'>Green</span>",
                "blue":"<span style='color:#0000FF;'>Blue</span>"
            }
        },
        {
            name:"selectItemMultipleGrid",title:"Select Multiple (Grid)",
            editorType:"SelectItem",
            multiple:true,
            multipleAppearance:"grid",
            valueMap:{
                "cat":"Cat",
                "dog":"Dog",
                "giraffe":"Giraffe",
                "goat":"Goat",
                "marmoset":"Marmoset",
                "mouse":"Mouse"
            }
        },
        {
            name:"selectItemMultiplePickList",title:"Select Multiple (PickList)",
            editorType:"SelectItem",
            multiple:true,
            multipleAppearance:"picklist",
            valueMap:{
                "cat":"Cat",
                "dog":"Dog",
                "giraffe":"Giraffe",
                "goat":"Goat",
                "marmoset":"Marmoset",
                "mouse":"Mouse"
            }
        }
        ]
    });
    isc.DynamicForm.create({
        ID:"dateForm",
        width:500,
        fixedColWidths:true,
        colWidths:[120,"*"],
        isGroup:true,
        groupTitle:"Date Controls",
        fields:[{
            name:"dateItem",title:"Date",hint:"<nobr>Picklist based date input</nobr>",
            editorType:"DateItem"
        },
        {
            name:"dateItem2",title:"Date",hint:"<nobr>Direct date input</nobr>",
            editorType:"DateItem",
            useTextField:true
        },
        {
            name:"timeItem",title:"Time",
            editorType:"TimeItem"
        },
        {
            name:"timeItem",title:"Time",hint:"Picklist based time input",
            editorType:"TimeItem",
            useTextField:false
        },
        {
            name:"dri",title:"Date Range",
            width:400,
            editorType:"DateRangeItem",
            allowRelativeDates:true,
            fromDate:"$today",
            toDate:"-1m"
        },
        {
            name:"mdri",title:"Mini Date Range",
            editorType:"MiniDateRangeItem"
        },
        {
            name:"rdi",title:"Relative Date",
            editorType:"RelativeDateItem"
        }
        ]
    });
    var departmentTree=isc.Tree.create({
        modelType:"children",
        root:{
            name:"root",
            children:[{
                id:1000,
                name:"Marketing",
                children:[
                    {id:1001,name:"Advertising"},
                    {id:1002,name:"Community Relations"}
                ]
            },{
                id:2000,
                name:"Sales",
                children:[
                    {id:2001,name:"Channel Sales"},
                    {id:2002,name:"Direct Sales"}
                ]
            },{
                id:3000,
                name:"Manufacturing",
                children:[
                    {id:3001,name:"Design"},
                    {id:3002,name:"Development"},
                    {id:3003,name:"QA"}
                ]
            },{
                id:4000,
                name:"Services",
                children:[
                    {id:4001,name:"Support"},
                    {id:4002,name:"Consulting"}
                ]
            }]
        }
    });
    isc.DataSource.create({
        ID:"clientOnlyUsersDS",
        clientOnly:true,
        fields:[{
            name:"id",
            title:"ID",
            type:"integer",
            primaryKey:true,
            hidden:true
        },{
            name:"name",
            title:"Name"
        }],
        cacheData:[
            {id:1,name:"Max"},
            {id:2,name:"Bethany"},
            {id:3,name:"Zach"},
            {id:4,name:"Francesca"}
        ]
    });
    isc.DataSource.create({
        ID:"clientOnlyUserFavoritesDS",
        clientOnly:true,
        fields:[{
            name:"id",
            title:"ID",
            type:"integer",
            primaryKey:true,
            hidden:true
        },{
            name:"userID",
            type:"integer",
            foreignKey:"clientOnlyUsersDS.id",
            title:"User ID"
        },{
            name:"favoriteAnimal",
            title:"Favorite Animal"
        },{
            name:"favoriteNumber",
            type:"number",
            title:"Favorite Integer (0 - 100)"
        },{
            name:"hasFavoriteColor",
            title:"Has a Favorite Color?",
            type:"boolean"
        },{
            name:"favoriteColor",
            valueMap:["Red","Orange","Yellow","Green","Blue","Indigo","Violet"]
        },{
            name:"favoriteDate",
            type:"date",
            title:"Favorite Date"
        },{
            name:"favoriteTime",
            type:"time",
            title:"Favorite Time"
        },{
            name:"favoriteMusicGenres",
            multiple:true,
            title:"Favorite Music Genres",
            valueMap:["Alternative","Classical","Country","Folk","Hip Hop","Jazz","Pop","R&B","Rock","World","Other"]
        },{
            name:"favoriteVacationDestination",
            title:"Favorite Vacation Destination",
            valueMap:{
                "AS":"Australia",
                "BR":"Brazil",
                "CA":"Canada",
                "CH":"China",
                "FR":"France",
                "GM":"Germany",
                "IN":"India",
                "ID":"Indonesia",
                "IT":"Italy",
                "JA":"Japan",
                "MX":"Mexico",
                "RS":"Russia",
                "KS":"South Korea",
                "SP":"Spain",
                "UK":"United Kingdom",
                "US":"United States"
            }
        },{
            name:"favoriteCuisines",
            multiple:true,
            title:"Favorite Cuisines",
            valueMap:{
                "US":"American",
                "AS":"Australian",
                "BR":"Brazilian",
                "UK":"British",
                "CA":"Canadian",
                "CH":"Chinese",
                "FR":"French",
                "GM":"German",
                "IN":"Indian",
                "ID":"Indonesian",
                "IT":"Italian",
                "JA":"Japanese",
                "KS":"Korean",
                "MX":"Mexican",
                "RS":"Russian",
                "SP":"Spanish"
            }
        },{
            name:"favoriteDepartment",
            title:"Favorite Department"
        }],
        cacheData:[
            {
                id:1,
                userID:1,
                favoriteAnimal:"Lemur",
                favoriteNumber:90,
                hasFavoriteColor:false,
                favoriteDate:isc.Date.createLogicalDate(2000,0,1),
                favoriteTime:isc.Date.createLogicalTime(0,0),
                favoriteMusicGenres:["Classical","Rock","World"],
                favoriteVacationDestination:"JA",
                favoriteCuisines:["CA","CH","IN","RS"],
                favoriteDepartment:4002
            },
            {
                id:2,
                userID:2,
                favoriteAnimal:"Zebra",
                favoriteNumber:12,
                hasFavoriteColor:true,
                favoriteColor:"Orange",
                favoriteDate:isc.Date.createLogicalDate(2012,11,12),
                favoriteTime:isc.Date.createLogicalTime(12,12),
                favoriteMusicGenres:["Alternative","Hip Hop"],
                favoriteVacationDestination:"US",
                favoriteCuisines:["US","AS","BR","UK"],
                favoriteDepartment:3002
            },
            {
                id:3,
                userID:3,
                favoriteAnimal:"Elephant",
                favoriteNumber:10,
                hasFavoriteColor:true,
                favoriteColor:"Green",
                favoriteDate:isc.Date.createLogicalDate(2010,9,10),
                favoriteTime:isc.Date.createLogicalTime(10,10),
                favoriteMusicGenres:["Country","Folk"],
                favoriteVacationDestination:"MX",
                favoriteCuisines:["GM","ID","JA","MX"],
                favoriteDepartment:1001
            },
            {
                id:4,
                userID:4,
                favoriteAnimal:"Blue Whale",
                favoriteNumber:55,
                hasFavoriteColor:true,
                favoriteColor:"Blue",
                favoriteDate:isc.Date.createLogicalDate(2005,4,5),
                favoriteTime:isc.Date.createLogicalTime(12,0),
                favoriteMusicGenres:["Pop","R&B","Other"],
                favoriteVacationDestination:"SP",
                favoriteCuisines:["FR","IT","KS","SP"],
                favoriteDepartment:3001
            }
        ]
    });
    var favoritesForm=isc.DynamicForm.create({
        ID:"favoritesForm",
        autoDraw:false,
        autoFetchData:true,
        initialCriteria:{userID:1},
        width:500,
        colWidths:[175,"*"],
        dataSource:clientOnlyUserFavoritesDS,
        revertValueKey:"Escape",
        items:[{
            name:"userID",
            title:"User",
            editorType:"SelectItem",
            optionDataSource:"clientOnlyUsersDS",
            valueField:"id",
            displayField:"name",
            changed:function(form,item,value){
                form.fetchData({userID:value},function(dsResponse,data,dsRequest){
                    var hasFavoriteColor=(data!=null&&data.length>=1&&
                                             !!data[0].hasFavoriteColor)
                    form.getItem("favoriteColor").setDisabled(!hasFavoriteColor);
                });
            }
        },{
            name:"favoriteAnimal",
            showPending:true
        },{
            name:"favoriteAnimal2",editorType:"TextAreaItem",height:40,
            showPending:true
        },{
            name:"favoriteNumber",
            editorType:"SliderItem",
            height:30,
            minValue:0,
            maxValue:100,
            showPending:true
        },{
            name:"hasFavoriteColor",
            editorType:"CheckboxItem",
            showPending:true,
            changed:function(form,item,value){
                form.getItem("favoriteColor").setDisabled(!value);
            }
        },{
            name:"favoriteColor",
            editorType:"RadioGroupItem",
            disabled:true,
            showPending:true
        },{
            name:"favoriteDate",
            editorType:"DateItem",
            showPending:true
        },{
            name:"favoriteTime",
            editorType:"TimeItem",
            showPending:true
        },{
            name:"favoriteMusicGenres",
            editorType:"MultiComboBoxItem",
            useInsertionOrder:false,
            showPending:true
        },{
            name:"favoriteVacationDestination",
            wrapTitle:false,
            editorType:"SelectItem",
            showPending:true,
            imageURLPrefix:"../../../isomorphic/client/reference/exampleImages/flags/16/",
            imageURLSuffix:".png",
            getValueIcon:function(value){
                return value;
            }
        },{
            name:"favoriteCuisines",
            editorType:"SelectItem",
            width:"*",
            showPending:true,
            showOldValueInHover:true,
            imageURLPrefix:"../../../isomorphic/client/reference/exampleImages/flags/16/",
            imageURLSuffix:".png",
            getValueIcon:function(value){
                if(isc.isAn.Array(value)){
                    return null;
                }
                return value;
            }
        },{
            name:"favoriteDepartment",
            editorType:"PickTreeItem",
            width:140,
            displayField:"name",
            valueField:"id",
            valueTree:departmentTree,
            showPending:true
        }]
    });
    favoritesForm.fetchData({userId:1},function(dsResponse,data){
        favoritesForm.setValue("favoriteAnimal","Changed value");
        favoritesForm.setValue("favoriteAnimal2","Changed value");
        favoritesForm.setValue("favoriteNumber",73);
        favoritesForm.setValue("hasFavoriteColor",false);
        favoritesForm.setValue("favoriteColor","Red");
        favoritesForm.setValue("favoriteDate",isc.Date.createLogicalDate(2012,11,12));
        favoritesForm.setValue("favoriteTime",isc.Date.createLogicalTime(12,12));
        favoritesForm.setValue("favoriteMusicGenres",["Country","Folk"]);
        favoritesForm.setValue("favoriteVacationDestination","US");
        favoritesForm.setValue("favoriteCuisines",["US","AS","BR","UK"]);
        favoritesForm.setValue("favoriteDepartment",3002);
        favoritesForm.setValue("favoriteMusicGenres",["Country","Folk","Jazz"]);
        favoritesForm.setValue("hasFavoriteColor",true);
        favoritesForm.getItem("favoriteColor").setDisabled(!favoritesForm.getValue("hasFavoriteColor"));
    });
    isc.HLayout.create({
        ID:"fiRowLayout1",
        membersMargin:20,
        members:[favoritesForm,form1,form2,dateForm]
    });
    isc.VLayout.create({
        ID:"formItemsPane",
        layoutMargin:10,
        membersMargin:20,
        top:60,
        autoDraw:false,
        members:[uiToolstripFI,fiRowLayout1]
    });
    return formItemsPane;
}
isc.getGridsPane=function(){
    isc.ListGrid.create({
        ID:"lgFilter",
        width:400,height:200,
        dataSource:worldDS,
        canEdit:true,
        fields:[
            {name:"countryName",title:"Country"},
            {name:"capital",title:"Capital"},
            {name:"continent",title:"Continent"}
        ],
        autoFetchData:true,
        showFilterEditor:true,
        initialCriteria:{fieldName:"countryName",operator:"iNotContains",value:"x"},
        initialSort:{property:"countryName",direction:"ascending"},
        selectionAppearance:"checkbox",
        dataArrived:function(){
            if(!this.__skipSelect){
                this.__skipSelect=true;
                this.selectRecord(this.data.find({countryName:"Albania"}));
            }
        }
    })
    isc.ListGrid.create({
        ID:"lgHeaderSpans",
        width:400,height:224,
        headerHeight:65,
        dataSource:worldDS,
        autoFetchData:true,
        dataFetchMode:"local",
        canEdit:true,
        initialSort:{property:"countryName",direction:"ascending"},
        showGridSummary:true,
        fields:[
            {name:"countryName",title:"Country"},
            {name:"capital"},
            {name:"government"},
            {name:"population",
                summaryFunction:"sum",
                showGridSummary:true
            }
        ],
        headerSpans:[
            {
                fields:["countryCode","countryName"],
                title:"Identification"
            },
            {
                fields:["capital","government","population"],
                title:"Government & Politics"
            }
        ],
        dataArrived:function(){
            if(!this.__skipSelect){
                this.__skipSelect=true;
                this.selectRecord(this.data.find({countryName:"Albania"}));
            }
        }
    });
    isc.ListGrid.create({
        ID:"lgExpandingRows",
        width:500,height:300,
        alternateRecordStyles:true,
        dataSource:supplyCategory,
        expansionFieldImageShowSelected:false,
        autoFetchData:true,
        canExpandRecords:true,
        expansionMode:"related",
        detailDS:"supplyItem",
        expansionRelatedProperties:{
            border:"1px solid inherit",
            margin:5,
            autoFitMaxRecords:3,
            fields:[
                {name:"itemName"},
                {name:"SKU"},
                {name:"description"}
            ]
        },
        dataArrived:function(){
            if(!this.__skipSelect){
                this.__skipSelect=true;
                this._allowGroup=true;
                this.expandRecord(this.data.get(1));
            }
        }
    });
    isc.ListGrid.create({
        ID:"lgGrouped",
        width:400,height:224,
        dataSource:worldDS,
        fields:[
            {name:"countryName",width:100},
            {name:"government"},
            {name:"population",width:100,
                summaryFunction:"sum",
                showGroupSummary:true
            }
        ],
        canEdit:true,
        groupStartOpen:"all",
        groupByField:'government',
        autoFetchData:true,
        initialSort:{property:"countryName",direction:"ascending"},
        showGroupSummary:true,
        dataArrived:function(){
            if(!this.__skipSelect){
                this.__skipSelect=true;
                this._allowGroup=true;
                this.selectRecord(this.data.find({countryName:"Albania"}));
            }
        },
        groupByComplete:function(){
            if(this._allowGroup){
                this._allowGroup=false;
                var record=this.originalData.find({countryName:"Albania"});
                this.selectRecord(record);
            }
        }
    });
    isc.ListGrid.create({
        ID:"lgGroupedHeaderSummaries",
        width:400,height:224,
        dataSource:worldDS,
        fields:[
            {name:"countryName",width:100},
            {name:"population",width:90,
                summaryFunction:"sum",
                showGroupSummary:true
            }
        ],
        canEdit:true,
        groupStartOpen:"all",
        groupByField:'government',
        autoFetchData:true,
        initialSort:{property:"countryName",direction:"ascending"},
        showGroupSummary:true,
        showGroupSummaryInHeader:true,
        dataArrived:function(){
            if(!this.__skipSelect){
                this.__skipSelect=true;
                this._allowGroup=true;
                this.selectRecord(this.data.find({countryName:"Albania"}));
            }
        },
        groupByComplete:function(){
            if(this._allowGroup){
                this._allowGroup=false;
                var record=this.originalData.find({countryName:"Albania"});
                this.selectRecord(record);
            }
        }
    });
    isc.TreeGrid.create({
        ID:"tgMultiColumn",
        width:400,
        height:300,
        dataSource:"employees",
        autoFetchData:true,
        nodeIcon:"icons/16/person.png",
        folderIcon:"icons/16/person.png",
        showOpenIcons:false,
        showDropIcons:false,
        closedIconSuffix:"",
        showSelectedIcons:true,
        dataProperties:{autoOpenRoot:true},
        fields:[
            {name:"Name"},
            {name:"Job"},
            {name:"Salary",formatCellValue:"isc.NumberUtil.format(value, '\u00A4,0.00')"}
        ]
    });
    isc.HLayout.create({
        ID:"lgRowLayout1",
        membersMargin:20,
        members:[lgFilter,lgHeaderSpans,lgExpandingRows]
    });
    isc.HLayout.create({
        ID:"lgRowLayout2",
        membersMargin:20,
        members:[lgGrouped,lgGroupedHeaderSummaries,tgMultiColumn]
    });
    isc.VLayout.create({
        ID:"gridsPane",
        layoutMargin:10,
        membersMargin:20,
        top:60,
        autoDraw:false,
        members:[lgRowLayout1,lgRowLayout2]
    });
    return gridsPane;
}
isc.getTabsPane=function(){
function getTabs(showTitles){
    var tabs=[
        {title:showTitles?"Edit":null,icon:"[SKINIMG]actions/edit.png",iconSize:16,
         pane:isc.Img.create({autoDraw:true,width:48,height:48,src:"[SKINIMG]actions/edit.png"})},
        {title:showTitles?"Save":null,icon:"[SKINIMG]actions/save.png",iconSize:16,canEditTitle:true,canClose:true,
         pane:isc.Img.create({autoDraw:true,width:48,height:48,src:"[SKINIMG]actions/save.png"})},
        {title:showTitles?"Approve":null,icon:"[SKINIMG]actions/approve.png",iconSize:16,disabled:true,showDisabledIcon:true,
         pane:isc.Img.create({autoDraw:true,width:48,height:48,src:"[SKINIMG]actions/approve.png"})},
        {title:showTitles?"A Particularly Long Title":null,icon:"[SKINIMG]actions/filter.png",iconSize:16,minWidth:200,
         pane:isc.Img.create({autoDraw:true,width:48,height:48,src:"[SKINIMG]actions/filter.png"})}
    ];
    return tabs;
}
isc.TabSet.create({
    ID:"tabSetTop",
    tabBarPosition:"top",
    width:540,
    height:100,
    titleEditEvent:"doubleClick",
    tabs:getTabs(true)
});
isc.TabSet.create({
    ID:"tabSetBottom",
    tabBarPosition:"bottom",
    width:540,
    height:100,
    titleEditEvent:"doubleClick",
    tabs:getTabs(true)
});
isc.TabSet.create({
    ID:"tabSetLeft",
    tabBarPosition:"left",
    width:200,
    height:300,
    defaultTabHeight:24,
    tabs:getTabs()
});
isc.TabSet.create({
    ID:"tabSetRight",
    tabBarPosition:"right",
    width:200,
    height:300,
    showTabTitle:false,
    defaultTabHeight:24,
    tabs:getTabs()
});
    isc.HLayout.create({
        ID:"tabsRowLayout1",
        membersMargin:20,
        members:[tabSetTop,tabSetBottom]
    });
    isc.HLayout.create({
        ID:"tabsRowLayout2",
        membersMargin:20,
        members:[tabSetLeft,tabSetRight]
    });
    isc.VLayout.create({
        ID:"tabsPane",
        layoutMargin:10,
        membersMargin:20,
        top:60,
        autoDraw:false,
        members:[tabsRowLayout1,tabsRowLayout2]
    });
    return tabsPane;
}
isc.getWidgetsPane=function(){
    isc.ListGrid.create({
        fields:[
            {type:"text",title:"System",name:"system"},
            {type:"text",title:"Monitor Name",name:"monitor"}
        ],
        ID:"lgTest1",
        canEdit:true,editEvent:"click",
        autoDraw:false
    })
    isc.HTMLFlow.create({
        ID:"statusReport",
        padding:5,border:"1px solid #808080",
        setNewStatus:function(system){
            this.setContents(system+
                ": <span style='color:green;font-weight:bold'>Normal</span><br>");
        }
    })
    isc.ImgButton.create({
        ID:"addButton",
        autoDraw:false,
        src:"[SKIN]actions/add.png",size:16,
        showFocused:false,showRollOver:false,showDown:false,
        click:"lgTest1.startEditingNew();return false;"
    });
    isc.ImgButton.create({
        ID:"removeButton",
        autoDraw:false,
        src:"[SKIN]actions/remove.png",size:16,
        showFocused:false,showRollOver:false,showDown:false,
        click:"lgTest1.removeSelectedData();return false;"
    });
    isc.DynamicForm.create({
        ID:"systemSelector",
        height:1,
        width:75,numCols:1,
        fields:[
            {name:"system",type:"select",width:150,showTitle:false,
             valueMap:["Development","Staging","Production"],
             defaultValue:"Development",
             change:"statusReport.setNewStatus(value)",
             click:"return false;"
            }
        ]
    });
    isc.SectionStack.create({
        ID:"sectionStack1",
        sections:[
            {items:lgTest1,title:"Monitors",controls:[addButton,removeButton],expanded:true},
            {items:statusReport,title:"Status",controls:systemSelector,expanded:true}
        ],
        visibilityMode:"multiple",
        animateSections:true,
        height:300,
        width:300,
        overflow:"hidden"
    })
    statusReport.setNewStatus(systemSelector.getValue("system"));
    isc.ColorPicker.create({
        ID:"colorPicker",
        autoDismiss:false
    });
    isc.DateChooser.create({
        ID:"dateChooserNormal"
    });
    isc.DateChooser.create({
        ID:"dateChooserFiscal",
        showWeekChooser:true,
        showFiscalYearChooser:true
    });
    isc.Menu.create({
        ID:"menuTest",
        autoDraw:true,
        autoDismiss:false,
        autoDismissOnBlur:false,
        data:[
            {title:"New",keyTitle:"Ctrl+N",icon:"icons/16/document_plain_new.png"},
            {title:"Open",keyTitle:"Ctrl+O",icon:"icons/16/folder_out.png"},
            {isSeparator:true},
            {title:"Save",keyTitle:"Ctrl+S",icon:"icons/16/disk_blue.png"},
            {title:"Save As",icon:"icons/16/save_as.png"},
            {isSeparator:true},
            {title:"Recent Documents",icon:"icons/16/folder_document.png",submenu:[
                {title:"data.xml",checked:true},
                {title:"Component Guide.doc"},
                {title:"SmartClient.doc",checked:true},
                {title:"AJAX.doc"}
            ]},
            {isSeparator:true},
            {title:"Export as...",icon:"icons/16/export1.png",submenu:[
                {title:"XML"},
                {title:"CSV"},
                {title:"Plain text"}
            ]},
            {isSeparator:true},
            {title:"Print",enabled:false,keyTitle:"Ctrl+P",icon:"icons/16/printer3.png"}
        ]
    });
    menuTest.show();
    isc.TabSet.create({
        ID:"tabSet1",
        tabBarPosition:"top",
        width:550,
        height:200,
        canEditTabTitles:true,
        titleEditEvent:"doubleClick",
        titleEditorTopOffset:2,
        tabs:[
            {title:"Blue",canClose:true,
             pane:isc.Img.create({autoDraw:false,width:48,height:48,src:"pieces/48/pawn_blue.png"})},
            {title:"Green",canClose:true,
             pane:isc.Img.create({autoDraw:false,width:48,height:48,src:"pieces/48/pawn_green.png"})},
            {title:"123-Yellow",canClose:true,ID:"validatedTab",
             pane:isc.Img.create({autoDraw:false,width:48,height:48,src:"pieces/48/pawn_yellow.png"})},
            {title:"Can't Change Me",canEditTitle:false,
             pane:isc.Img.create({autoDraw:false,width:48,height:48,src:"pieces/48/pawn_red.png"})}
        ],
        titleChanged:function(newTitle,oldTitle,tab){
            if(tab.ID=="validatedTab"&&(!newTitle||newTitle.substring(0,4)!="123-")){
                isc.warn("Tab title must start with the prefix \"123-\"");
                return false;
            }
        }
    });
    isc.RichTextEditor.create({
        ID:"rteNormal",
        height:200
    });
    isc.HLayout.create({
        ID:"widgetsRowLayout1",
        membersMargin:20,
        members:[sectionStack1,dateChooserNormal,dateChooserFiscal,menuTest,colorPicker]
    });
    isc.HLayout.create({
        ID:"widgetsRowLayout2",
        membersMargin:20,
        members:[rteNormal]
    });
    isc.VLayout.create({
        ID:"widgetsPane",
        layoutMargin:10,
        membersMargin:20,
        top:60,
        autoDraw:false,
        members:[widgetsRowLayout1,tabSet1,widgetsRowLayout2]
    });
    return widgetsPane;
}
isc.getWindowsPane=function(){
    isc.defineClass("HelpCanvas","Canvas").addProperties({
        autoDraw:false,
        defaultWidth:300,
        padding:10,
        contents:"<b>Severity 1</b> - Critical problem<br>System is unavailable in production or "+
                  "is corrupting data, and the error severely impacts the user's operations."+
                  "<br><br><b>Severity 2</b> - Major problem<br>An important function of the system "+
                  "is not available in production, and the user's operations are restricted."+
                  "<br><br><b>Severity 3</b> - Minor problem<br>Inability to use a function of the "+
                  "system occurs, but it does not seriously affect the user's operations."
    });
    isc.Window.create({
        ID:"winNormal",
        title:"Auto-sizing window",
        autoSize:true,
        canDragReposition:true,
        canDragResize:true,
        items:[
            isc.HelpCanvas.create()
        ]
    });
    isc.Window.create({
        ID:"winScrollbar",
        width:200,
        height:200,
        title:"Normal window",
        canDragReposition:true,
        canDragResize:true,
        items:[
            isc.HelpCanvas.create()
        ]
    });
    isc.Window.create({
        ID:"winFooter",
        title:"Window with footer",
        width:300,height:200,
        canDragResize:true,
        showFooter:true,
        items:[
            isc.Label.create({
                contents:"Click me",
                align:"center",
                padding:5,
                height:"100%",
                click:function(){
                    winFooter.setStatus("Click at: "+isc.EventHandler.getX()+", "+isc.EventHandler.getY());
                }
            })
        ]
    });
    isc.MultiSortDialog.create({
        ID:"multiSort",
        fields:isc.getValues(animals.getFields()),
        initialSort:[{property:"Animal",direction:"ascending"}]
    });
    isc.MultiGroupDialog.create({
        ID:"multiGroup",
        fields:isc.getValues(animals.getFields())
    });
    isc.HLayout.create({
        ID:"winRowLayout1",
        membersMargin:20,
        members:[winNormal,winScrollbar,winFooter]
    });
    isc.HLayout.create({
        ID:"winRowLayout2",
        membersMargin:20,
        members:[multiSort,multiGroup]
    });
    isc.VLayout.create({
        ID:"windowsPane",
        layoutMargin:10,
        membersMargin:20,
        top:60,
        autoDraw:false,
        members:[winRowLayout1,winRowLayout2]
    });
    return windowsPane;
}
isc.getCalendarsPane=function(){
    var _today=new Date;
    var _start=_today.getDate()-_today.getDay();
    var _month=_today.getMonth();
    var _year=_today.getFullYear();
    var eventData=[
        {
            eventId:1,
            name:"Meeting",
            description:"Shareholders meeting: monthly forecast report",
            startDate:new Date(_year,_month,_start+2,9),
            endDate:new Date(_year,_month,_start+2,14)
        },
        {
            eventId:2,
            name:"Realtor",
            description:"This canvas is styled by color settings on the CalendarEvent",
            startDate:new Date(_year,_month,_start+3,8),
            endDate:new Date(_year,_month,_start+3,10),
            headerTextColor:"black",
            headerBackgroundColor:"orange",
            headerBorderColor:"darkorange",
            textColor:"darkgreen",
            borderColor:"darkorange",
            backgroundColor:"#ffffcc"
        },
        {
            eventId:3,
            name:"Soccer",
            description:"Little league soccer finals",
            startDate:new Date(_year,_month,_start+4,13),
            endDate:new Date(_year,_month,_start+4,16)
        },
        {
            eventId:4,
            name:"Sleep",
            description:"Catch up on sleep",
            startDate:new Date(_year,_month,_start+4,5),
            endDate:new Date(_year,_month,_start+4,9)
        },
        {
            eventId:5,
            name:"Inspection",
            description:"This canvas is styled and disabled by custom styleName and canEdit settings on the CalendarEvent",
            startDate:new Date(_year,_month,_start+4,10),
            endDate:new Date(_year,_month,_start+4,12),
            styleName:"testStyle",
            canEdit:false
        },
        {
            eventId:6,
            name:"Airport run",
            description:"This canvas is styled by color settings on the CalendarEvent",
            startDate:new Date(_year,_month,_start+4,1),
            endDate:new Date(_year,_month,_start+4,3),
            headerTextColor:"white",
            headerBackgroundColor:"green",
            headerBorderColor:"green",
            textColor:"darkgreen",
            borderColor:"darkgreen",
            backgroundColor:"lightgreen"
        },
        {
            eventId:7,
            name:"Dinner Party",
            description:"Prepare elaborate meal for friends",
            startDate:new Date(_year,_month,_start+4,17),
            endDate:new Date(_year,_month,_start+4,20)
        },
        {
            eventId:8,
            name:"Poker",
            description:"Poker at Steve's house",
            startDate:new Date(_year,_month,_start+4,21),
            endDate:new Date(_year,_month,_start+4,23)
        },
        {
            eventId:9,
            name:"Meeting",
            description:"Board of directors meeting: discussion of next months strategy",
            startDate:new Date(_year,_month,_start+5,11),
            endDate:new Date(_year,_month,_start+5,15)
        }
    ];
    isc.DataSource.create({
        ID:"eventDS",
        fields:[
            {name:"eventId",primaryKey:true,type:"sequence"},
            {name:"name"},
            {name:"description"},
            {name:"startDate",type:"datetime"},
            {name:"endDate",type:"datetime"}
        ],
        clientOnly:true,
        testData:eventData
    });
var _today=new Date();
var y=_today.getFullYear();
var m=_today.getMonth();
var d=_today.getDate();
var dayLaneData=[
{
    eventId:1,
    startDate:new Date(y,m,d,8,0),
    endDate:new Date(y,m,d,12,0),
    name:"Development Meeting",
    description:"This canvas is styled and disabled by custom styleName and canEdit settings on the CalendarEvent",
    lane:"charlesMadigen",
    styleName:"testStyle",
    canEdit:false
},
{
    eventId:2,
    startDate:new Date(y,m,d,14,0),
    endDate:new Date(y,m,d,18,0),
    name:"Mgmt Meeting",
    description:"Management Meeting",
    lane:"charlesMadigen"
},
{
    eventId:3,
    startDate:new Date(y,m,d,4,0),
    endDate:new Date(y,m,d,12,0),
    name:"Data Cleansing",
    description:"This canvas is styled by color settings on the CalendarEvent",
    lane:"tamaraKane",
    headerTextColor:"black",
    headerBackgroundColor:"orange",
    headerBorderColor:"darkorange",
    textColor:"darkgreen",
    borderColor:"darkorange",
    backgroundColor:"#ffffcc"
},
{
    eventId:4,
    startDate:new Date(y,m,d,7,30),
    endDate:new Date(y,m,d,12,0),
    name:"Data Migration",
    description:"Data Migration",
    lane:"tamaraKane"
},
{
    eventId:5,
    startDate:new Date(y,m,d,13,0),
    endDate:new Date(y,m,d,16,0),
    name:"Documentation",
    description:"Product documentation",
    lane:"tamaraKane"
},
{
    eventId:6,
    startDate:new Date(y,m,d,8,0),
    endDate:new Date(y,m,d,20,0),
    name:"Support",
    description:"Telephone Support",
    lane:"darcyFeeney"},
{
    eventId:7,
    startDate:new Date(y,m,d,11,0),
    endDate:new Date(y,m,d,11,30),
    name:"Break",
    description:"Morning break",
    lane:"darcyFeeney"
},
{
    eventId:8,
    startDate:new Date(y,m,d,16,0),
    endDate:new Date(y,m,d,16,30),
    name:"Break",
    description:"Afternoon break",
    lane:"darcyFeeney"
},
{
    eventId:9,
    startDate:new Date(y,m,d,0,0),
    endDate:new Date(y,m,d,8,0),
    name:"Premium Support",
    description:"This canvas is styled by color settings on the CalendarEvent",
    lane:"kaiKong",
    headerTextColor:"white",
    headerBackgroundColor:"green",
    headerBorderColor:"green",
    textColor:"darkgreen",
    borderColor:"darkgreen",
    backgroundColor:"lightgreen"
},
{
    eventId:10,
    startDate:new Date(y,m,d,20,0),
    endDate:new Date(y,m,d,23,59),
    name:"Support",
    description:"Premium Support (overnight response)",
    lane:"kaiKong"
},
{
    eventId:11,
    startDate:new Date(y,m,d,8,0),
    endDate:new Date(y,m,d,12,0),
    name:"Development Meeting",
    description:"Development Meeting",
    lane:"shellyFewel"
},
{
    eventId:12,
    startDate:new Date(y,m,d,13,30),
    endDate:new Date(y,m,d,17,30),
    name:"Team Meeting",
    description:"Developer Team Meeting",
    lane:"shellyFewel"
}
];
    isc.Calendar.create({
        ID:"calDay",width:500,height:300,
        autoDraw:false,
        dataSource:eventDS,autoFetchData:true,
        showWeekView:false,
        showMonthView:false
    });
    isc.Calendar.create({
        ID:"calWeek",width:500,height:300,
        autoDraw:false,
        dataSource:eventDS,autoFetchData:true,
        showDayView:false,
        showMonthView:false
    });
    isc.Calendar.create({
        ID:"calMonth",width:500,height:300,
        autoDraw:false,
        dataSource:eventDS,autoFetchData:true,
        showDayView:false,
        showWeekView:false
    });
    var lanes=[
        {name:"charlesMadigen",title:"Charles Madigen"},
        {name:"tamaraKane",title:"Tamara Kane"},
        {name:"darcyFeeney",title:"Darcy Feeney"},
        {name:"kaiKong",title:"Kai Kong"},
        {name:"shellyFewel",title:"Shelly Fewel"}
    ];
    isc.Calendar.create({
        ID:"calDayLanes",
        width:400,height:300,
        autoDraw:false,
        data:dayLaneData,
        lanes:lanes,
        showWeekView:false,
        showMonthView:false,
        showTimelineView:false,
        chosenDate:new Date(),
        showDayLanes:true,
        canEditLane:true
    });
    isc.Calendar.create({
        ID:"calCompact",
        width:400,
        height:220,
        autoDraw:false,
        showDayView:false,
        showWeekView:false,
        showOtherDays:false,
        showDayHeaders:false,
        showDateChooser:false,
        showDatePickerButton:false,
        showAddEventButton:false,
        disableWeekends:false,
        dataSource:eventDS,
        autoFetchData:true,
        canCreateEvents:false,
        getDayBodyHTML:function(date,events,calendar,rowNum,colNum){
            returnStr=date.getDate()+" ";
            if(events&&events.length>0){
                returnStr+=this.imgHTML("icons/16/approved.png",16,16,"image");
            }
            return returnStr;
        },
        dayBodyClick:function(date,events,calendar,rowNum,colNum){
            var nameStr="";
            if(events.length==0)nameStr="No events";
            for(var i=0;i<events.length;i++){
                nameStr+=events[i].name+"<BR/>";
            }
            isc.say(nameStr,{title:date.toUSShortDate()});
        }
    });
    isc.Calendar.create({
        ID:"calWorkDay",width:400,height:300,
        autoDraw:false,
        dataSource:eventDS,autoFetchData:true,
        showDayView:false,
        showMonthView:false,
        showWeekends:false,
        showWorkday:true,
        scrollToWorkday:true
    });
    isc.HLayout.create({
        ID:"calendarRowLayout1",
        membersMargin:20,
        members:[calDay,calWeek,calMonth]
    });
    isc.HLayout.create({
        ID:"calendarRowLayout2",
        membersMargin:20,
        members:[calDayLanes,calWorkDay,calCompact]
    });
    isc.VLayout.create({
        ID:"calendarsPane",
        layoutMargin:10,
        membersMargin:20,
        top:60,
        autoDraw:false,
        members:[calendarRowLayout1,calendarRowLayout2]
    });
    return calendarsPane;
}
isc.getTimelinesPane=function(){
    var developers=[
        {name:"charlesMadigen",title:"Charles Madigen",devGroup:"Managers"},
        {name:"tamaraKane",title:"Tamara Kane",devGroup:"Developers"},
        {name:"darcyFeeney",title:"Darcy Feeney",devGroup:"Managers"},
        {name:"kaiKong",title:"Kai Kong",devGroup:"Developers"},
        {name:"shellyFewel",title:"Shelly Fewel",devGroup:"Managers"},
        {name:"garretMonroe",title:"Garret Monroe",devGroup:"Developers"}
    ];
    var _calStart=isc.DateUtil.getStartOf(new Date(2016,6,5),"W");
    var _calEnd=_calStart.duplicate();
    _calEnd.setDate(_calEnd.getDate()+20);
    isc.Timeline.create({
        ID:"timeline1",
        height:451,
        width:500,
        autoDraw:false,
        startDate:_calStart,
        endDate:_calEnd,
        dataSource:tasks,
        autoFetchData:true,
        lanes:developers,
        headerLevels:[{unit:"week"},{unit:"day"}],
        laneFields:[{name:"title",title:"Developer",minWidth:120,autoFitWidth:true}],
        canEditLane:true,
        showEventDescriptions:false,
        columnsPerPage:5
    });
    var _calStart=isc.DateUtil.getStartOf(new Date(2016,6,5),"W");
    var _calEnd=_calStart.duplicate();
    _calEnd.setDate(_calEnd.getDate()+20);
    isc.Timeline.create({
        ID:"timelineGrouped",
        width:500,
        height:451,
        autoDraw:false,
        startDate:_calStart,
        endDate:_calEnd,
        dataSource:"tasks",
        lanes:developers,
        headerLevels:[{unit:"week"},{unit:"day"}],
        canEditLane:true,
        showEventDescriptions:false,
        columnsPerPage:5,
        laneEventPadding:2,
        disableWeekends:false,
        canGroupLanes:true,
        laneGroupByField:"devGroup",
        laneFields:[
            {name:"title",title:"Developer",minWidth:120,autoFitWidth:true},
            {name:"devGroup",hidden:true}
        ]
    });
    isc.HLayout.create({
        ID:"timelineRowLayout1",
        membersMargin:20,
        members:[timeline1,timelineGrouped]
    });
    isc.HLayout.create({
        ID:"timelineRowLayout2",
        membersMargin:20,
        members:[]
    });
    isc.VLayout.create({
        ID:"timelinesPane",
        layoutMargin:10,
        membersMargin:20,
        top:60,
        autoDraw:false,
        members:[timelineRowLayout1,timelineRowLayout2]
    });
    return timelinesPane;
}
isc._debugModules = (isc._debugModules != null ? isc._debugModules : []);isc._debugModules.push('SkinUtil');isc.checkForDebugAndNonDebugModules();isc._moduleEnd=isc._SkinUtil_end=(isc.timestamp?isc.timestamp():new Date().getTime());if(isc.Log&&isc.Log.logIsInfoEnabled('loadTime'))isc.Log.logInfo('SkinUtil module init time: ' + (isc._moduleEnd-isc._moduleStart) + 'ms','loadTime');delete isc.definingFramework;if (isc.Page) isc.Page.handleEvent(null, "moduleLoaded", { moduleName: 'SkinUtil', loadTime: (isc._moduleEnd-isc._moduleStart)});}else{if(window.isc && isc.Log && isc.Log.logWarn)isc.Log.logWarn("Duplicate load of module 'SkinUtil'.");}
/*

  SmartClient Ajax RIA system
  Version v12.0p_2018-08-09/EVAL Development Only (2018-08-09)

  Copyright 2000 and beyond Isomorphic Software, Inc. All rights reserved.
  "SmartClient" is a trademark of Isomorphic Software, Inc.

  LICENSE NOTICE
     INSTALLATION OR USE OF THIS SOFTWARE INDICATES YOUR ACCEPTANCE OF
     ISOMORPHIC SOFTWARE LICENSE TERMS. If you have received this file
     without an accompanying Isomorphic Software license file, please
     contact licensing@isomorphic.com for details. Unauthorized copying and
     use of this software is a violation of international copyright law.

  DEVELOPMENT ONLY - DO NOT DEPLOY
     This software is provided for evaluation, training, and development
     purposes only. It may include supplementary components that are not
     licensed for deployment. The separate DEPLOY package for this release
     contains SmartClient components that are licensed for deployment.

  PROPRIETARY & PROTECTED MATERIAL
     This software contains proprietary materials that are protected by
     contract and intellectual property law. You are expressly prohibited
     from attempting to reverse engineer this software or modify this
     software for human readability.

  CONTACT ISOMORPHIC
     For more information regarding license rights and restrictions, or to
     report possible license violations, please contact Isomorphic Software
     by email (licensing@isomorphic.com) or web (www.isomorphic.com).

*/

